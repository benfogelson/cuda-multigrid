//  Copyright Joel de Guzman 2002-2004. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//  Hello World Example from the tutorial
//  [Joel de Guzman 10/9/2002]

#include <boost/python/detail/wrap_python.hpp>
#include <numpy/arrayobject.h>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/extract.hpp>

using namespace boost::python;

template<typename T>
T * data_ptr_from_object(object A_obj){
    PyArrayObject * A_ptr = (PyArrayObject*)(A_obj.ptr());
    return (T*)A_ptr->data;
}

template <typename T>
void do_interpolate(object vF_obj, object vC_obj){
    int nF = extract<int>(vF_obj.attr("shape")[0]);
    int nC = extract<int>(vC_obj.attr("shape")[0]);

    T * vF = data_ptr_from_object<T>(vF_obj);
    T * vC = data_ptr_from_object<T>(vC_obj);

    int iF, jF, iC, jC, di, dj;

    for(iF = 1; iF < nF - 1; ++iF){
        for(jF = 1; jF < nF - 1; ++jF){
            vF[iF * nF + jF] = 0;
        }
    }
    for(iC = 0; iC < nC; ++iC){
        iF = 2*iC;
        for(jC = 0; jC < nC; ++jC){
            jF = 2*jC;
            for(di = -1; di <= 1; ++di){
                for(dj = -1; dj <= 1; ++dj){
                    if(0 < iF + di && iF + di < nF - 1 && 0 < jF + dj && jF + dj < nF - 1){
                        vF[(iF + di) * nF + (jF + dj)] +=
                                (di == 0 ? 1 : 0.5) * (dj == 0 ? 1 : 0.5) * vC[iC * nC + jC];
                    }
                }
            }
        }
    }
}


template <typename T>
void do_restrict(object vC_obj, object vF_obj) {
    int nF = extract<int>(vF_obj.attr("shape")[0]);
    int nC = extract<int>(vC_obj.attr("shape")[0]);

    T * vF = data_ptr_from_object<T>(vF_obj);
    T * vC = data_ptr_from_object<T>(vC_obj);

    int iF, jF, iC, jC;

    for(iC = 1; iC < nC - 1; ++iC){
        iF = 2*iC;
        for(jC = 1; jC < nC - 1; ++jC) {
            jF = 2 * jC;
            vC[iC * nC + jC] = 4.0 * vF[iF * nF + jF]
                               + 2.0 * (vF[(iF + 1) * nF + jF] + vF[(iF - 1) * nF + jF]
                                             + vF[iF * nF + jF + 1] + vF[iF * nF + jF - 1])
                               + vF[(iF + 1) * nF + jF + 1] + vF[(iF - 1) * nF + jF + 1]
                               + vF[(iF + 1) * nF + jF - 1] + vF[(iF - 1) * nF + jF - 1];
            vC[iC * nC + jC] /= 16.0;
        }
    }

}

template <typename T>
void smooth_weighted_jacobi(object u_obj, object f_obj, object g_obj, object its_obj){
    T w = 4.0/5.0;

    T h = (T)(extract<double>(g_obj.attr("_h64")));
    T h2 = h*h;

    int N = extract<int>(g_obj.attr("_N64"));

    int its = extract<int>(its_obj);

    object ucurr_obj = extract<object>(u_obj.attr("curr"));
    object utmp_obj = extract<object>(u_obj.attr("tmp"));

    T * ucurr = data_ptr_from_object<T>(ucurr_obj);
    T * utmp = data_ptr_from_object<T>(utmp_obj);
    T * f = data_ptr_from_object<T>(f_obj);
    T * uswap;

    int clock, i, j;
    for(clock = 0; clock < its; ++clock){
        for(i = 1; i < N - 1; ++i){
            for(j = 1; j < N - 1; ++j){
                utmp[i * N + j] = w * 0.25 * (h2 * f[i * N + j] + ucurr[(i+1) * N + j] + ucurr[(i-1) * N + j]
                                             + ucurr[i * N + j + 1] + ucurr[i * N + j - 1])
                        + (1.0 - w) * ucurr[i * N + j];
            }
        }
        u_obj.attr("swap")();
        uswap = ucurr;
        ucurr = utmp;
        utmp = uswap;
    }
}


template <typename T>
void apply_minus_laplacian(object Lu_obj, object u_obj, object g_obj){
    T h = (T) (extract<double>(g_obj.attr("_h64")));
    T h2inv = 1.0/(h * h);

    int N = extract<int>(g_obj.attr("_N64"));

    T * Lu = data_ptr_from_object<T>(Lu_obj);
    T * u = data_ptr_from_object<T>(u_obj);

    int i, j;
    for (i = 1; i < N - 1; ++i) {
        for (j = 1; j < N - 1; ++j) {
            Lu[i * N + j] = h2inv*(4.0 * u[i * N + j] - u[(i - 1) * N + j]
                                   - u[(i + 1) * N + j] - u[i * N + j - 1] - u[i * N + j + 1]);
        }
    }
}


template <typename T>
void compute_residual(object r_obj, object u_obj, object f_obj, object g_obj){
    T h = (T) (extract<double>(g_obj.attr("_h64")));
    T h2inv = 1.0/(h * h);

    int N = extract<int>(g_obj.attr("_N64"));

    T * r = data_ptr_from_object<T>(r_obj);
    T * u = data_ptr_from_object<T>(u_obj);
    T * f = data_ptr_from_object<T>(f_obj);

    int i, j;
    for(i = 0; i < N; ++i){
        for(j = 0; j < N; ++j){
            r[i * N + j] = 0;
        }
    }

    for(i = 1; i < N - 1; ++i){
        for(j = 1; j < N - 1; ++j){
            r[i * N + j] = f[i * N + j] - h2inv*(4.0 * u[i * N + j] - u[(i - 1) * N + j] - u[(i + 1) * N + j] - u[i * N + j - 1] - u[i * N + j + 1]);
        }
    }
}


BOOST_PYTHON_MODULE(_multigrid_cxx){
    def("interpolate_double", do_interpolate<double>);
    def("interpolate_single", do_interpolate<float>);
    def("restrict_double", do_restrict<double>);
    def("restrict_single", do_restrict<float>);
    def("smooth_weighted_jacobi_double", smooth_weighted_jacobi<double>);
    def("smooth_weighted_jacobi_single", smooth_weighted_jacobi<float>);
    def("apply_minus_laplacian_double", apply_minus_laplacian<double>);
    def("apply_minus_laplacian_single", apply_minus_laplacian<float>);
    def("compute_residual_double", compute_residual<double>);
    def("compute_residual_single", compute_residual<float>);
}

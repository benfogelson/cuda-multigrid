import multigrid as mg
import numpy as np
import argparse
import logging
import os.path
import time


# Using code example from here: https://www.huyng.com/posts/python-performance-analysis
class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            print 'elapsed time: %f ms' % self.msecs

logDotLine = '..........'

def run_main(args):
    logger = logging.getLogger('timing.run_main')

    methods = set(args.method)

    startupTimes = {}
    solveTimes = {}
    copybackTimes = {}

    for m in methods:
        logger_m = logger.getChild(m + '\t')
        logger_m.info(logDotLine)
        device = True if m == 'device' else False
        dtype = np.float32 if (m == 'device' or m.endswith('32')) else np.float64
        compiled = False if m.find('cxx') == -1 else True

        startupTime = 0.0

        try:
            with Timer() as t:
                solver = mg.MultigridSolver(dtype, device, compiled)
            logger_m.info('Initialized multigrid solver with dtype = %s, device = %s, compiled = %s'\
                          % (str(dtype), str(device), str(compiled)))
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            with Timer() as t:
                solver.initialize_hierarchy(args.ncoarsest, args.numgrids)
            infostr = 'Initialized hierarchy with N = '
            for level in solver.fullHierarchy[:-1]:
                infostr += str(level.grid.N) + ', '
            infostr += str(solver.fullHierarchy[-1].grid.N)
            logger_m.info(infostr)
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            with Timer() as t:
                solver.set_initial_solution(1)
            logger_m.info('Set initial solution')
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            with Timer() as t:
                solver.set_boundary_condition(0)
            logger_m.info('Set boundary condition')
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            with Timer() as t:
                solver.set_rhs(0)
            logger_m.info('Set right hand side')
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            with Timer() as t:
                solver.set_vcycle_parameters(args.presmooths, args.postsmooths, args.coarsesmooths)
            logger_m.info('Set solver to use %i presmooths, %i postsmooths, and %i coarse grid smooths'
                          % (args.presmooths, args.postsmooths, args.coarsesmooths))
            logger_m.info('...%f seconds elapsed' % t.secs)
            startupTime += t.secs

            startupTimes[m] = startupTime

            if solver.is_ready():
                logger_m.info('Solver is ready')
            else:
                logger_m.error('Solver is not ready')
                raise RuntimeError('solver.is_ready() returned False')

            logger_m.info('Beginning multigrid solve with %i v cycles' % args.vcycles)
            with Timer() as t:
                solver.run_vcycle(args.vcycles)
            logger_m.info('Finished multigrid solve')
            logger_m.info('...%f seconds elapsed' % t.secs)
            solveTimes[m] = t.secs

            if device:
                with Timer() as t:
                    solver.memcpy_dtoh_finest((mg.U,))
                logger_m.info('Copied solution back to host')
                logger_m.info('...%f seconds elapsed' % t.secs)
                copybackTime = t.secs
            else:
                copybackTime = 0
            copybackTimes[m] = copybackTime
        except Exception as e:
            logger_m.error(e.message)
            startupTimes[m] = np.nan
            solveTimes[m] = np.nan
            copybackTimes[m] = np.nan

    for m in solveTimes:
        infostr = '\n   %s:' % m
        infostr += '\n      %i cycles in %f seconds, for an average of %f sec/cycle'\
                    % (args.vcycles, solveTimes[m], solveTimes[m]/args.vcycles)
        infostr += '\n      %f seconds of initialization and setup time' % startupTimes[m]
        infostr += '\n      %f seconds of postsolve time' % copybackTimes[m]

        tot = startupTimes[m] + solveTimes[m] + copybackTimes[m]
        infostr += '\n      total run time of %f, for an average of %f sec/cycle' % (tot, tot/args.vcycles)
        logger.info(infostr)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--method',
                        help='multigrid methods to test',
                        choices=['hostpy32', 'hostpy64', 'hostcxx32', 'hostcxx64', 'device']
                        , action='append')
    parser.add_argument('-s1', '--presmooths',
                        help='number of presmooths',
                        type=int, default=3)
    parser.add_argument('-s2', '--postsmooths',
                        help='number of postsmooths',
                        type=int, default=3)
    parser.add_argument('-sc', '--coarsesmooths',
                        help='number of smooths to use for exact solve on coarsest grid',
                        type=int, default=100)
    parser.add_argument('-vc', '--vcycles',
                        help='number of vcycles to run for timing test',
                        type=int, default=10)
    parser.add_argument('-nc', '--ncoarsest',
                        help='number of grid points in one direction on coarsest grid',
                        type=int,
                        default=5)
    parser.add_argument('-g', '--numgrids',
                        help='number of grids',
                        type=int, default=4)
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='report log on command line in addition to file',
                        default=False)
    parser.add_argument('-l', '--logfile',
                        help='log file name',
                        default='timing.log')
    args = parser.parse_args()


    # Using this SO answer:
    # http://stackoverflow.com/questions/9321741/printing-to-screen-and-writing-to-a-file-at-the-same-time
    # define a Handler which writes INFO messages or higher to the sys.stderr
    logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=os.path.abspath(args.logfile))
    if args.verbose:
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        # set a format which is simpler for console use
        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        # tell the handler to use this format
        console.setFormatter(formatter)
        # add the handler to the root logger
        logging.getLogger('').addHandler(console)

    infostr = '\ntiming_runs.py called with options:'
    for a in args.__dict__:
        infostr += ('\n   %s = %s' % (str(a), str(args.__dict__[a])))
    logging.info(infostr)

    run_main(args)
    argparse.Namespace
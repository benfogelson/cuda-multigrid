from types import *

from .grid import Grid
from .smoother import ArrayPair
from . import boundaryconditions as bc
from . import smoother as sm
from . import operator as op
from .grid import intergrid as ig
from . import cudautil as cu

import numpy as np

import compiledhost as ch

U = 0
F = 1
LU = 2
E = 3
R = 4


class GridLevelData(object):
    def __init__(self, grid, u, f, Lu, e, r):
        self.grid = grid
        self.u = u
        self.f = f
        self.Lu = Lu
        self.e = e
        self.r = r


class GridLevelDataDevice(GridLevelData):
    def __init__(self, hostdata, u_d, f_d, Lu_d, e_d, r_d, N_d, h_d):
        self.grid = hostdata.grid
        self.u = hostdata.u
        self.f = hostdata.f
        self.Lu = hostdata.Lu
        self.e = hostdata.e
        self.r = hostdata.r
        self.u_d = u_d
        self.f_d = f_d
        self.Lu_d = Lu_d
        self.e_d = e_d
        self.r_d = r_d
        self.N_d = N_d
        self.h_d = h_d


def create_grid_level_host(N, dtype, grid = None):
    g = grid if grid is not None else Grid(N, dtype)

    ucurr = g.create_grid_variable(0)
    utmp = g.create_grid_variable(0)
    u = ArrayPair(ucurr, utmp)
    f = g.create_grid_variable(0)
    Lu = g.create_grid_variable(0)
    e = g.create_grid_variable(0)
    r = g.create_grid_variable(0)

    data = GridLevelData(g, u, f, Lu, e, r)

    return data


def create_grid_level_device(N, grid = None):
    dtype = np.float32
    g = grid if grid is not None else Grid(N, dtype)
    assert g.dtype == dtype

    hostdata = create_grid_level_host(N, dtype, g)

    nbytes = g.get_grid_variable_nbytes()
    ucurr_d = cu.cuda.mem_alloc(nbytes)
    utmp_d = cu.cuda.mem_alloc(nbytes)
    u_d = ArrayPair(ucurr_d, utmp_d)
    f_d = cu.cuda.mem_alloc(nbytes)
    Lu_d = cu.cuda.mem_alloc(nbytes)
    e_d = cu.cuda.mem_alloc(nbytes)
    r_d = cu.cuda.mem_alloc(nbytes)
    N_d = cu.int_to_device(g.N)
    h_d = cu.float_to_device(g.h)

    cu.cuda.memcpy_htod(ucurr_d, hostdata.u.curr)
    cu.cuda.memcpy_htod(utmp_d, hostdata.u.tmp)
    cu.cuda.memcpy_htod(f_d, hostdata.f)
    cu.cuda.memcpy_htod(Lu_d, hostdata.Lu)
    cu.cuda.memcpy_htod(e_d, hostdata.e)
    cu.cuda.memcpy_htod(r_d, hostdata.r)

    data = GridLevelDataDevice(hostdata, u_d, f_d, Lu_d, e_d, r_d, N_d, h_d)

    return data


def create_grid_level_host_and_device(N, grid = None):
    dtype = np.float32
    g = grid if grid is not None else Grid(N, dtype)
    assert g.dtype == dtype

    hostdata = create_grid_level_host(N, dtype, g)
    devicedata = create_grid_level_device(N, g)

    return hostdata, devicedata


class MultigridSolver(object):
    def __init__(self, dtype, device, compiled=False):
        self._hierarchyInitialized = False
        self._hostDataInitialized = False
        self._deviceDataInitialized = False
        self._functionsInitialized = False
        self._bcSet = False
        self._vcycleParamsSet = False
        self._initialSolutionSet = False
        self._rhsSet = False
        self.compiled = compiled

        self._debug_zero_residual = False

        assert dtype in (float, FloatType, np.float32, np.float64)
        assert isinstance(device, BooleanType)

        if dtype == FloatType or dtype == float:
            self.dtype = np.float64
        else:
            self.dtype = dtype

        if self.dtype != np.float32 and device:
            raise RuntimeError('Device requires single precision arithmetic.')

        self.device = device

        if self.device:
            self._initialize_device_functions()
        else:
            self._initialize_host_functions()

    def run_vcycle(self, num):
        assert num >= 0
        assert isinstance(num, IntType)
        assert self.is_ready()

        level = self.fullHierarchy[0]

        if not self.device:
            for k in xrange(num):
                self._vcycle_host(self.fullHierarchy)
        else:
            for k in xrange(num):
                self._vcycle_device(self.fullHierarchy)

    def _vcycle_device(self, hierarchy):
        if len(hierarchy) != 1:
            finelevel = hierarchy[-1]
            coarselevel = hierarchy[-2]

            for _ in xrange(self.presmooths):
                self.smooth(finelevel.u_d.curr,
                            finelevel.u_d.tmp,
                            finelevel.f_d,
                            finelevel.h_d,
                            finelevel.N_d,
                            block=cu.BLOCK_SIZE,
                            grid=cu.compute_grid(finelevel.u.curr))
                finelevel.u_d.swap()
                finelevel.u.swap()
            self.compute_residual(finelevel.r_d,
                                  finelevel.u_d.curr,
                                  finelevel.f_d,
                                  finelevel.h_d,
                                  finelevel.N_d,
                                  block=cu.BLOCK_SIZE,
                                  grid=cu.compute_grid(finelevel.u.curr))
            if __debug__:
                if self._debug_zero_residual:
                    logging.debug('Setting residual to 0 for testing purposes. To stop this, turn off Solver._debug_zero_residual.')
                    op._zero_array_func(finelevel.r_d,
                                        finelevel.N_d,
                                        block=cu.BLOCK_SIZE,
                                        grid=cu.compute_grid(finelevel.r))
            self.restrict(coarselevel.f_d,
                          finelevel.r_d,
                          coarselevel.N_d,
                          finelevel.N_d,
                          block=cu.BLOCK_SIZE,
                          grid=cu.compute_grid(coarselevel.u.curr))
            self._vcycle_device(hierarchy[:-1])
            self.interpolate(finelevel.e_d,
                             coarselevel.u_d.curr,
                             finelevel.N_d,
                             coarselevel.N_d,
                             block=cu.BLOCK_SIZE,
                             grid=cu.compute_overlapped_grid(coarselevel.u.curr),
                             shared=cu.INTERGRID_SHARED_MEMORY_SIZE)
            self.correct_error(finelevel.u_d.curr,
                               finelevel.e_d,
                               finelevel.N_d,
                               block=cu.BLOCK_SIZE,
                               grid=cu.compute_grid(finelevel.u.curr))
            for _ in xrange(self.postsmooths):
                self.smooth(finelevel.u_d.curr,
                            finelevel.u_d.tmp,
                            finelevel.f_d,
                            finelevel.h_d,
                            finelevel.N_d,
                            block=cu.BLOCK_SIZE,
                            grid=cu.compute_grid(finelevel.u.curr))
                finelevel.u_d.swap()
                finelevel.u.swap()
            op._zero_array_func(coarselevel.u_d.curr,
                                coarselevel.N_d,
                                block=cu.BLOCK_SIZE,
                                grid=cu.compute_grid(coarselevel.u.curr))
        else:
            level = hierarchy[0]
            for _ in xrange(self.coarsesmooths):
                self.smooth(level.u_d.curr,
                            level.u_d.tmp,
                            level.f_d,
                            level.h_d,
                            level.N_d,
                            block=cu.BLOCK_SIZE,
                            grid=cu.compute_grid(level.u.curr))
                level.u_d.swap()
                level.u.swap()

    def _vcycle_host(self, hierarchy):
        if len(hierarchy) != 1:
            finelevel = hierarchy[-1]
            coarselevel = hierarchy[-2]

            self.smooth(finelevel.u, finelevel.f, finelevel.grid, self.presmooths)
            self.compute_residual(finelevel.r, finelevel.u.curr, finelevel.f, finelevel.grid)
            if __debug__:
                if self._debug_zero_residual:
                    logging.debug('Setting residual to 0 for testing purposes. To stop this, turn off Solver._debug_zero_residual.')
                    finelevel.r[:,:] = 0
            self.restrict(coarselevel.f, finelevel.r)
            self._vcycle_host(hierarchy[:-1])
            self.interpolate(finelevel.e, coarselevel.u.curr)
            finelevel.u.curr[:,:] = finelevel.u.curr + finelevel.e
            self.smooth(finelevel.u, finelevel.f, finelevel.grid, self.postsmooths)

            coarselevel.u.curr[:,:] = 0
        else:
            level = hierarchy[0]
            self.smooth(level.u, level.f, level.grid, self.coarsesmooths)

    def is_ready(self):
        return (
                self._functionsInitialized and
                self._hierarchyInitialized and
                self._initialSolutionSet and
                self._rhsSet and
                self._bcSet and
                self._vcycleParamsSet
        )

    def set_vcycle_parameters(self, presmooths = 3, postsmooths = 3, coarsesmooths = 100):
        for k in (presmooths, postsmooths, coarsesmooths):
            assert isinstance(k, IntType)
            assert k >= 0

        self.presmooths = presmooths
        self.postsmooths = postsmooths
        self.coarsesmooths = coarsesmooths

        self._vcycleParamsSet = True

    def initialize_hierarchy(self, Ncoarsest, numGrids):
        if self.device:
            def make_generator():
                N = Ncoarsest
                while True:
                    yield create_grid_level_device(N)
                    N = 2 * N - 1
            gen = make_generator()
        else:
            def make_generator():
                N = Ncoarsest
                while True:
                    yield create_grid_level_host(N, self.dtype)
                    N = 2 * N - 1
            gen = make_generator()

        self.fullHierarchy = [gen.next() for _ in xrange(numGrids)]

        self._hierarchyInitialized = True
        self._bcSet = False
        self._initialSolutionSet = False
        self._rhsSet = False

    def set_initial_solution(self, u0):
        if not self._hierarchyInitialized:
            raise RuntimeError('Need to initialize grid hierarchy before setting initial solution.')

        level = self.fullHierarchy[-1]
        grid = level.grid
        ucurr = level.u.curr
        utmp = level.u.tmp
        grid.set_grid_variable(ucurr, u0)
        grid.set_grid_variable(utmp, u0)

        for level in self.fullHierarchy[:-1]:
            grid = level.grid
            ucurr = level.u.curr
            utmp = level.u.tmp
            grid.set_grid_variable(ucurr, 0)
            grid.set_grid_variable(utmp, 0)

        if not self.device:
            self._initialSolutionSet = True
            self._bcSet = False
            self._rhsSet = True
        else:
            # Do not copy initial solution to device here, because
            # we still need to set boundary conditions
            self._initialSolutionSet = True
            self._bcSet = False
            self._rhsSet = True

    def set_rhs(self, rhs):
        if not self._hierarchyInitialized:
            raise RuntimeError('Need to initialize grid hierarchy before setting rhs.')

        level = self.fullHierarchy[-1]
        grid = level.grid
        f = level.f
        grid.set_grid_variable(f, rhs)

        for level in self.fullHierarchy[:-1]:
            grid = level.grid
            f = level.f
            grid.set_grid_variable(f, 0)

        if not self.device:
            self._rhsSet = True
        else:
            for level in self.fullHierarchy:
                f = level.f
                f_d = level.f_d
                cu.cuda.memcpy_htod(f_d, f)
            self._rhsSet = True

    def set_boundary_condition(self, bdry, locs = Grid.ALL_BOUNDARIES):
        if not self._hierarchyInitialized:
            raise RuntimeError('Need to initialize grid hierarchy before setting boundary condition.')

        if not self._initialSolutionSet:
            raise RuntimeError('Need to initialize initial solution before setting boundary condition.')

        level = self.fullHierarchy[-1]
        grid = level.grid
        ucurr = level.u.curr
        utmp = level.u.tmp
        bc.apply_dirichlet_boundary_condition(ucurr, grid, bdry, locs)
        bc.apply_dirichlet_boundary_condition(utmp, grid, bdry, locs)

        for level in self.fullHierarchy[:-1]:
            grid = level.grid
            ucurr = level.u.curr
            utmp = level.u.tmp
            bc.apply_dirichlet_boundary_condition(ucurr, grid, 0)
            bc.apply_dirichlet_boundary_condition(utmp, grid, 0)

        if not self.device:
            self._bcSet = True
        else:
            for level in self.fullHierarchy:
                u = level.u
                u_d = level.u_d
                cu.cuda.memcpy_htod(u_d.curr, u.curr)
                cu.cuda.memcpy_htod(u_d.tmp, u.tmp)
            self._bcSet = True

    def _initialize_device_functions(self):
        if self._functionsInitialized:
            raise RuntimeError('Multigrid functions were already initialized for device.')

        self._interpolate_func = ig._interpolate_func
        self._restrict_func = ig._restrict_func
        self._smooth_func = sm._smooth_weighted_jacobi_func
        self._compute_residual_func = op._compute_residual_func
        self._correct_error_func = op._correct_error_func

        self._functionsInitialized = True

    def _initialize_host_functions(self):
        if self._functionsInitialized:
            raise RuntimeError('Multigrid functions were already initialized for host.')

        if not self.compiled:
            self._interpolate_func = ig.interpolate
            self._restrict_func = ig.restrict
            self._smooth_func = sm.smooth_weighted_jacobi
            self._compute_residual_func = op.compute_residual
        else:
            if self.dtype == np.float64:
                self._interpolate_func = ch.interpolate_double
                self._restrict_func = ch.restrict_double
                self._smooth_func = ch.smooth_weighted_jacobi_double
                self._compute_residual_func = ch.compute_residual_double
            elif self.dtype == np.float32:
                self._interpolate_func = ch.interpolate_single
                self._restrict_func = ch.restrict_single
                self._smooth_func = ch.smooth_weighted_jacobi_single
                self._compute_residual_func = ch.compute_residual_single

        self._functionsInitialized = True

    def interpolate(self, *args, **kwargs):
        self._interpolate_func(*args, **kwargs)

    def restrict(self, *args, **kwargs):
        self._restrict_func(*args, **kwargs)

    def smooth(self, *args, **kwargs):
        self._smooth_func(*args, **kwargs)

    def compute_residual(self, *args, **kwargs):
        self._compute_residual_func(*args, **kwargs)

    def correct_error(self, *args, **kwargs):
        self._correct_error_func(*args, **kwargs)

    def memcpy_dtoh_all(self, targets=(U, F, LU, R, E)):
        for level in self.fullHierarchy:
            self.memcpy_dtoh(level, targets)

    def memcpy_dtoh_finest(self, targets=(U,)):
        self.memcpy_dtoh(self.fullHierarchy[-1], targets)

    def memcpy_dtoh(self, level, targets=(U,)):
        if U in targets:
            cu.cuda.memcpy_dtoh(level.u.curr, level.u_d.curr)
            cu.cuda.memcpy_dtoh(level.u.tmp, level.u_d.tmp)
        if F in targets:
            cu.cuda.memcpy_dtoh(level.f, level.f_d)
        if LU in targets:
            cu.cuda.memcpy_dtoh(level.Lu, level.Lu_d)
        if R in targets:
            cu.cuda.memcpy_dtoh(level.r, level.r_d)
        if E in targets:
            cu.cuda.memcpy_dtoh(level.e, level.e_d)

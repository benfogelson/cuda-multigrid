import numpy as np
from . import cudautil as cu
from . import grid

mod = cu.SourceModule("""
    __global__ void apply_minus_laplacian(float * u, float * Lu, float * h_ptr, int * N_ptr){
        int i = blockDim.y*blockIdx.y + threadIdx.y;
        int j = blockDim.x*blockIdx.x + threadIdx.x;

        const float & h = h_ptr[0];
        const int & N = N_ptr[0];

        const float h2inv = 1.0/(h*h);

        if(i < N && j < N){
            if(1 <= i && i < N - 1 && 1 <= j && j < N - 1){
                Lu[i*N + j] = -h2inv*(u[(i+1)*N + j] +
                                u[(i-1)*N + j] + u[i*N + (j+1)] + u[i*N + (j-1)] - 4*u[i*N + j]);
            }
            else{
                Lu[i*N + j] = 0;
            }
        }
    }

    __global__ void compute_residual(float * r, float * u, float * f, float * h_ptr, int * N_ptr){
        int i = blockDim.y*blockIdx.y + threadIdx.y;
        int j = blockDim.x*blockIdx.x + threadIdx.x;

        const float & h = h_ptr[0];
        const int & N = N_ptr[0];

        const float h2inv = 1.0/(h*h);

        if(i < N && j < N){
            if(1 <= i && i < N - 1 && 1 <= j && j < N - 1){
                r[i*N + j] = f[i*N + j] - h2inv*(4*u[i*N + j] - u[(i+1)*N + j] -
                                u[(i-1)*N + j] - u[i*N + (j+1)] - u[i*N + (j-1)]);
            }
            else{
                r[i*N + j] = 0;
            }
        }
    }

    __global__ void correct_error(float * u, float * e, int * N_ptr){
        int i = blockDim.y*blockIdx.y + threadIdx.y;
        int j = blockDim.x*blockIdx.x + threadIdx.x;

        const int & N = N_ptr[0];

        if(1 <= i && i < N - 1 && 1 <= j && j < N - 1){
            u[i*N + j] += e[i*N + j];
        }
    }

    __global__ void zero_array(float * u, int * N_ptr){
        int i = blockDim.y*blockIdx.y + threadIdx.y;
        int j = blockDim.x*blockIdx.x + threadIdx.x;

        const int & N = N_ptr[0];

        if(i < N && j && j < N){
            u[i*N + j] = 0;
        }
    }
    """)
_apply_minus_laplacian_func = mod.get_function('apply_minus_laplacian')
_compute_residual_func = mod.get_function('compute_residual')
_correct_error_func = mod.get_function('correct_error')
_zero_array_func = mod.get_function('zero_array')


def compute_residual(r, u, f, g):
    assert u.ndim == 2
    assert u.shape == (g.N, g.N)
    assert r.shape == u.shape
    assert f.shape == u.shape

    assert u.dtype == g.dtype
    assert r.dtype == u.dtype
    assert f.dtype == u.dtype

    assert isinstance(g.h, g.dtype)

    r[:, :] = 0

    h2 = g.h ** 2

    for i in xrange(1, g.N-1):
        for j in xrange(1, g.N-1):
            r[i, j] = f[i, j] - (1.0/(h2))*(4*u[i, j] - u[i + 1, j] - u[i - 1, j] - u[i, j + 1] - u[i, j - 1])


def _laplacian_asserts(Lu, u, g):
    assert u.ndim == 2
    assert u.shape == (g.N, g.N)
    assert Lu.shape == u.shape
    assert u.dtype == g.dtype
    assert Lu.dtype == u.dtype
    assert isinstance(g.h, g.dtype)


def apply_minus_laplacian_device(u, Lu, g):
    _laplacian_asserts(Lu, u, g)

    assert g.dtype == np.float32

    h = g.h
    N = g.N

    u_d = cu.cuda.mem_alloc(u.nbytes)
    Lu_d = cu.cuda.mem_alloc(Lu.nbytes)

    cu.cuda.memcpy_htod(u_d, u)

    _apply_minus_laplacian_func(u_d,
                                Lu_d,
                                cu.float_to_device(h),
                                cu.int_to_device(N),
                                block = cu.BLOCK_SIZE,
                                grid = cu.compute_grid(u))

    cu.cuda.memcpy_dtoh(Lu, Lu_d)




    # _restrict_asserts(vC, vF)
    #
    # nF, _ = vF.shape
    # nC, _ = vC.shape
    #
    # vF_d = cu.cuda.mem_alloc(vF.nbytes)
    # vC_d = cu.cuda.mem_alloc(vC.nbytes)
    #
    # cu.cuda.memcpy_htod(vF_d, vF)
    #
    # _restrict_func(vC_d, vF_d, cu.int_to_device(nC), cu.int_to_device(nF),
    #                block = cu.BLOCK_SIZE, grid = cu.compute_grid(vC))
    #
    # cu.cuda.memcpy_dtoh(vC, vC_d)

def apply_minus_laplacian(Lu, u, g):
    _laplacian_asserts(Lu, u, g)
    h = g.h
    N = g.N

    h2inv = 1.0/(h ** 2.0)

    for i in xrange(1, N-1):
        for j in xrange(1, N-1):
            Lu[i,j] = -(u[i+1, j] + u[i-1, j] + u[i, j+1] + u[i, j-1] - 4*u[i, j])*h2inv

def apply_minus_laplacian_premultiplied(Lu, u, g):
    _laplacian_asserts(Lu, u, g)
    N = g.N

    for i in xrange(1, N-1):
        for j in xrange(1, N-1):
            Lu[i, j] = 4*u[i, j] - (u[i+1, j] + u[i-1, j] + u[i, j+1] + u[i, j-1])

from .. import smoother as sm
from ..smoother import ArrayPair
from .. import boundaryconditions as bc
from ..grid import Grid
from .. import operator as op

import numpy as np
from numpy.testing import assert_array_equal, assert_array_almost_equal
from nose.tools import assert_false, assert_raises

from nose.plugins.attrib import attr

from .. import compiledhost as ch

@attr('slow')
class _TestJacobiSmoother(object):
    @classmethod
    def setup_class(cls):
        cls.g = Grid(32, cls.dtype)

        cls.maxIts = 10000

    def test_zero_iterations_unchanged(self):
        ucurr = self.g.create_grid_variable()
        ucurr[:,:] = np.random.rand(self.g.N, self.g.N)
        utmp = ucurr.copy()

        u0 = ucurr.copy()

        u = ArrayPair(ucurr, utmp)

        f = self.g.create_grid_variable()
        f[:,:] = np.random.rand(self.g.N, self.g.N)

        self.smooth_jacobi(u, f, self.g, 0)

        assert_array_equal(u.curr, u0)
        assert_array_equal(u.tmp, u0)

    def test_one_iteration_swaps(self):
        ucurr = self.g.create_grid_variable()
        ucurr[:,:] = np.random.rand(self.g.N, self.g.N)
        utmp = ucurr.copy()

        u0 = ucurr.copy()

        u = ArrayPair(ucurr, utmp)

        f = self.g.create_grid_variable()
        f[:,:] = np.random.rand(self.g.N, self.g.N)

        self.smooth_jacobi(u, f, self.g, 1)

        assert_array_equal(u.tmp, u0)
        assert_false(np.array_equal(u.curr, u0))

    def test_smoother_converges_to_zero_soln(self):
        ucurr = self.g.create_grid_variable()
        ucurr[:,:] = np.random.rand(self.g.N, self.g.N)
        bc.apply_dirichlet_boundary_condition(ucurr, self.g, 0)
        utmp = ucurr.copy()
        u = ArrayPair(ucurr, utmp)

        f = self.g.create_grid_variable(0)

        self.smooth_jacobi(u, f, self.g, self.maxIts)

        assert_array_almost_equal(u.curr, 0, decimal=10)

    def test_smoother_converges_to_known_soln(self):
        rhs_func = lambda x, y: np.power(np.pi,2)*(3*np.cos(np.pi*x) + 16*np.sin(4*np.pi*y))
        u_func = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)

        ucurr = self.g.create_grid_variable(0)
        bc.apply_dirichlet_boundary_condition(ucurr, self.g, u_func)
        utmp = ucurr.copy()
        u = ArrayPair(ucurr, utmp)

        uexact = self.g.create_grid_variable()
        uexact[:,:] = u_func(self.g.X, self.g.Y)

        f = self.g.create_grid_variable()
        f[:,:] = rhs_func(self.g.X, self.g.Y)

        self.smooth_jacobi(u, f, self.g, self.maxIts)

        assert_array_almost_equal(u.curr, uexact, decimal=1)

    def test_fixed_point(self):
        u_func = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)
        uexact = self.g.create_grid_variable(u_func)
        f = self.g.create_grid_variable(0)
        self.apply_Lu(f, uexact, self.g)
        ucurr = uexact.copy()
        utmp = ucurr.copy()
        u = ArrayPair(ucurr, utmp)

        self.smooth_jacobi(u, f, self.g, 1)

        assert_array_almost_equal(u.curr, uexact, decimal=(6 if self.dtype == np.float32 else 15))


class TestArrayPair(object):
    def test_requires_identical_types(self):
        A = np.ones((10,10))
        B = 'I am a string'

        assert_raises(AssertionError, ArrayPair, A, B)

    def test_requires_same_dtypes(self):
        A = np.ones((10,10), dtype=np.float64)
        B = 2*np.ones((10,10), dtype=np.float32)

        assert_raises(AssertionError, ArrayPair, A, B)

    def test_requires_same_shapes(self):
        A = np.ones((10,10), dtype=np.float64)
        B = 2*np.ones((9,9), dtype=np.float64)

        assert_raises(AssertionError, ArrayPair, A, B)

    def test_initializes(self):
        A = np.ones((10,10), dtype=np.float64)
        B = 2*np.ones((10,10), dtype=np.float64)

        pair = ArrayPair(A, B)

        assert_array_equal(pair.curr, 1)
        assert_array_equal(pair.tmp, 2)

    def test_swaps(self):
        A = np.ones((10,10), dtype=np.float64)
        B = 2*np.ones((10,10), dtype=np.float64)
        pair = ArrayPair(A, B)

        pair.swap()

        assert_array_equal(pair.curr, 2)
        assert_array_equal(pair.tmp, 1)


class TestJacobiSmootherHostDouble(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.smooth_jacobi = staticmethod(sm.smooth_weighted_jacobi)
        cls.apply_Lu = staticmethod(op.apply_minus_laplacian)

        super(TestJacobiSmootherHostDouble, cls).setup_class()


class TestJacobiSmootherHostDoubleCompiled(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.smooth_jacobi = staticmethod(ch.smooth_weighted_jacobi_double)
        cls.apply_Lu = staticmethod(ch.apply_minus_laplacian_double)

        super(TestJacobiSmootherHostDoubleCompiled, cls).setup_class()


class TestJacobiSmootherHostSingle(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.smooth_jacobi = staticmethod(sm.smooth_weighted_jacobi)
        cls.apply_Lu = staticmethod(op.apply_minus_laplacian)

        super(TestJacobiSmootherHostSingle, cls).setup_class()


class TestJacobiSmootherHostDoubleCompiled(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.smooth_jacobi = staticmethod(ch.smooth_weighted_jacobi_single)
        cls.apply_Lu = staticmethod(ch.apply_minus_laplacian_single)

        super(TestJacobiSmootherHostDoubleCompiled, cls).setup_class()

class TestJacobiSmootherHostSingleCompiled(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.smooth_jacobi = staticmethod(ch.smooth_weighted_jacobi_single)
        cls.apply_Lu = staticmethod(ch.apply_minus_laplacian_single)

        super(TestJacobiSmootherHostSingleCompiled, cls).setup_class()


class TestJacobiSmootherDevice(_TestJacobiSmoother):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.smooth_jacobi = staticmethod(sm.smooth_weighted_jacobi_device)
        cls.apply_Lu = staticmethod(op.apply_minus_laplacian_device)

        super(TestJacobiSmootherDevice, cls).setup_class()

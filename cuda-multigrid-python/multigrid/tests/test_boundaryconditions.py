import numpy as np

from ..grid import Grid

from .. import boundaryconditions as bc

from nose.tools import assert_equals
from numpy.testing import assert_array_equal

class _TestDirichletBoundaryConditions(object):
    @classmethod
    def setup_class(cls):
        cls.g = Grid(256, cls.dtype)

    def test_zero_bc(self):
        u = self.g.create_grid_variable()
        u[:, :] = np.random.rand(self.g.N, self.g.N)

        bc.apply_dirichlet_boundary_condition(u, self.g, 0)

        n, e, s, w = Grid.get_all_boundary_values(u)

        assert_array_equal(n, 0, 'North BC failed')
        assert_array_equal(e, 0, 'East BC failed')
        assert_array_equal(s, 0, 'South BC failed')
        assert_array_equal(w, 0, 'West BC failed')


    def test_constant_bc(self):
        u = self.g.create_grid_variable()
        u[:, :] = np.random.rand(self.g.N, self.g.N)

        c = 4.1

        bc.apply_dirichlet_boundary_condition(u, self.g, c)

        n, e, s, w = Grid.get_all_boundary_values(u)

        assert_array_equal(n, c, 'North BC failed')
        assert_array_equal(e, c, 'East BC failed')
        assert_array_equal(s, c, 'South BC failed')
        assert_array_equal(w, c, 'West BC failed')

    def test_discontinuous_constant_bc(self):
        u = self.g.create_grid_variable()
        u[:, :] = np.random.rand(self.g.N, self.g.N)

        cn, ce, cs, cw = np.random.rand(4)

        bc.apply_dirichlet_boundary_condition(u, self.g, cn, Grid.NORTH)
        bc.apply_dirichlet_boundary_condition(u, self.g, ce, Grid.EAST)
        bc.apply_dirichlet_boundary_condition(u, self.g, cs, Grid.SOUTH)
        bc.apply_dirichlet_boundary_condition(u, self.g, cw, Grid.WEST)

        n, e, s, w = Grid.get_all_boundary_values(u)

        assert_array_equal(n[1:-1], cn, 'North BC failed')
        assert_array_equal(e[1:-1], ce, 'East BC failed')
        assert_array_equal(s[1:-1], cs, 'South BC failed')
        assert_array_equal(w[1:-1], cw, 'West BC failed')

    def test_function_bc(self):
        u = self.g.create_grid_variable()
        u[:, :] = np.random.rand(self.g.N, self.g.N)
        func = lambda x, y: np.sin(x * y)

        bc.apply_dirichlet_boundary_condition(u, self.g, func)

        n, e, s, w = Grid.get_all_boundary_values(u)
        nx, ex, sx, wx = Grid.get_all_boundary_values(self.g.X)
        ny, ey, sy, wy = Grid.get_all_boundary_values(self.g.Y)

        assert_array_equal(n[1:-1], func(nx, ny)[1:-1])
        assert_array_equal(e[1:-1], func(ex, ey)[1:-1])
        assert_array_equal(s[1:-1], func(sx, sy)[1:-1])
        assert_array_equal(w[1:-1], func(wx, wy)[1:-1])

class TestDirichletBoundaryConditionsHostDouble(_TestDirichletBoundaryConditions):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64

        super(TestDirichletBoundaryConditionsHostDouble, cls).setup_class()

class TestDirichletBoundaryConditionsHostSingle(_TestDirichletBoundaryConditions):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32

        super(TestDirichletBoundaryConditionsHostSingle, cls).setup_class()
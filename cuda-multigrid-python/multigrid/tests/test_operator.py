from .. import cudautil as cu
import numpy as np

from .. import grid

from numpy.testing import assert_array_almost_equal, assert_array_equal

from nose.tools import assert_equals, assert_almost_equals
from nose.plugins.skip import SkipTest
from nose.plugins.attrib import attr

from .. import operator as op

class _TestLaplaceOperator(object):
    @classmethod
    def setup_class(cls):
        cls.g = grid.Grid(N = cls.N, dtype = cls.dtype)

        cls.decimalPlaces = 8 if cls.dtype == np.float64 else 4


    @classmethod
    def teardown_class(cls):
        pass

    def test_constant_has_zero_laplacian(self):
        u = self.g.create_grid_variable(1.5)
        Lu = self.g.create_grid_variable()

        self.apply_L(u, Lu, self.g)

        assert_array_almost_equal(Lu[1:self.g.N-1, 1:self.g.N-1], 0, self.decimalPlaces)

    def test_trig_function_minus_laplacian(self):
        f = lambda x, y: 3*np.cos(np.pi * x) + np.sin(4 * np.pi * y)
        Lf = lambda x, y: 3*(np.pi ** 2)*np.cos(np.pi * x)\
                          + 16*(np.pi ** 2)*np.sin(4 * np.pi * y)
        u = f(self.g.X, self.g.Y)
        LuExact = Lf(self.g.X, self.g.Y)
        Lu = self.g.create_grid_variable(0)

        self.apply_L(u, Lu, self.g)

        LuExact[:,0] = 0
        LuExact[0,:] = 0
        LuExact[:,-1] = 0
        LuExact[-1,:] = 0

        err = self.g.compute_grid_norm(Lu - LuExact, np.Inf)

        assert_almost_equals(err, 0, delta=0.2)

    def test_linear_function_has_zero_laplacian(self):
        if self.dtype == np.float32:
            raise SkipTest()

        u = self.g.create_grid_variable()
        Lu = self.g.create_grid_variable()
        X = self.g.X
        Y = self.g.Y
        linear_func = lambda x, y: self.g.float(7.2*x - 4.3*y + 6.1)
        u[:,:] = linear_func(X, Y)

        self.apply_L(u, Lu, self.g)

        assert_array_almost_equal(Lu[1:self.g.N-1, 1:self.g.N-1], 0, self.decimalPlaces)

    def test_quadratic_function_has_constant_laplacian(self):
        if self.dtype == np.float32:
            raise SkipTest()

        u = self.g.create_grid_variable()
        Lu = self.g.create_grid_variable()
        X = self.g.X
        Y = self.g.Y
        quadratic_func = lambda x, y:\
            self.g.float(3.5 * (x ** 2)
                         - 2.1 * (y ** 2)
                         + 4.5 * (x * y)
                         + 7.1 * x
                         + 5.9 * y
                         - 12.1
                         )
        u[:,:] = quadratic_func(X, Y)

        self.apply_L(u, Lu, self.g)

        assert_array_almost_equal(Lu[1:self.g.N-1, 1:self.g.N-1], -2.8, self.decimalPlaces)

    @attr('slow')
    def test_order_of_convergence(self):
        if self.dtype == np.float32:
            raise SkipTest()

        normOrder = np.Inf
        startPower = 7
        numGrids = 4

        assert numGrids > 2

        f = lambda x, y: 3*np.cos(np.pi * x) + np.sin(4 * np.pi * y)
        Lf = lambda x, y: 3*(np.pi ** 2)*np.cos(np.pi * x)\
                          + 16*(np.pi ** 2)*np.sin(4 * np.pi * y)

        Nlist = map(lambda k: 2**(startPower+k), range(numGrids))
        gridList = map(lambda N: grid.Grid(N, self.dtype), Nlist)
        uList = map(lambda g: f(g.X, g.Y), gridList)
        LuList = map(lambda u: np.zeros_like(u), uList)
        def zero_edges(A):
            A[:,0] = 0
            A[:,-1] = 0
            A[0,:] = 0
            A[-1,:] = 0
            return A

        LuExactList = map(lambda g: zero_edges(Lf(g.X, g.Y)), gridList)

        map(lambda u, Lu, g: self.apply_L(u, Lu, g), uList, LuList, gridList)
        norms = np.array(map(lambda Lu, LuExact, g:
                             g.compute_grid_norm(Lu - LuExact, normOrder),
                             LuList, LuExactList, gridList))
        normRatios = norms[1:]/norms[:-1]

        fit = np.polyfit(Nlist[:-1], normRatios, 1)
        errorReduction = fit[1]

        assert_almost_equals(errorReduction, 0.25, places=2)


class TestLaplaceOperatorHostDouble(_TestLaplaceOperator):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.N = 256
        cls.apply_L = staticmethod(op.apply_minus_laplacian)

        super(TestLaplaceOperatorHostDouble, cls).setup_class()


class TestLaplaceOperatorDeviceSingle(_TestLaplaceOperator):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.N = 256
        cls.apply_L = staticmethod(op.apply_minus_laplacian_device)

        super(TestLaplaceOperatorDeviceSingle, cls).setup_class()

from .. import cudautil as cu
import numpy as np

from nose.tools import assert_equals, assert_almost_equals

def test_float_to_device():
    x = .49204829

    x_d = cu.float_to_device(x)

    xOut = np.empty(1, dtype=np.float32)
    cu.cuda.memcpy_dtoh(xOut, x_d)

    assert_almost_equals(x, xOut)

def test_int_to_device():
    N = 12345

    N_d = cu.int_to_device(N)
    Nout = np.empty(1, dtype=np.int32)
    cu.cuda.memcpy_dtoh(Nout, N_d)

    assert N == Nout

def test_compute_overlap_grid_full_block():
    block = (19, 15, 1)
    A = np.ones((155, 158))
    overlap = 2
    gridAnswer = (9, 12)

    grid = cu.compute_overlapped_grid(A, block, overlap)

    assert_equals(grid, gridAnswer)

def test_compute_overlap_grid_nonsquare_block():
    block = (19, 15, 1)
    A = np.ones((100, 43))
    overlap = 2
    gridAnswer = (6, 4)

    grid = cu.compute_overlapped_grid(A, block, overlap)

    assert_equals(grid, gridAnswer)

def test_compute_overlap_grid_square_block():
    block = (16,16,1)
    A = np.ones((64, 32))
    overlap = 2
    gridAnswer = (5, 3)

    grid = cu.compute_overlapped_grid(A, block, overlap)

    assert_equals(grid, gridAnswer)

def test_compute_zero_overlap_grid_square_block():
    block = (16,16,1)
    A = np.ones((50, 30))
    overlap = 0
    gridAnswer = cu.compute_grid(A, block)

    grid = cu.compute_overlapped_grid(A, block, overlap)

    assert_equals(grid, gridAnswer)

def test_compute_zero_overlap_grid_nonsquare_block():
    block = (32, 16, 1)
    A = np.ones((66, 38))
    overlap = 0
    gridAnswer = cu.compute_grid(A, block)

    grid = cu.compute_overlapped_grid(A, block, overlap)

    assert_equals(grid, gridAnswer)


def test_compute_grid_square_block():
    block = (16,16, 1)
    A = np.ones((50, 30))

    grid = cu.compute_grid(A, block)

    assert_equals(grid, (4, 2))

def test_compute_grid_nonsquare_block():
    block = (32, 16, 1)
    A = np.ones((66, 38))

    grid = cu.compute_grid(A, block)

    assert_equals(grid, (3, 3))

def test_compute_grid_full_block():
    block = (32, 16, 1)
    A = np.ones((64, 160))

    grid = cu.compute_grid(A, block)

    assert_equals(grid, (2, 10))
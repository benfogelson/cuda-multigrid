from .. import multigrid as mg

from .. import smoother as sm

import numpy as np
from ..import operator as op

from ..grid import Grid

from .. import cudautil as cu

from nose.plugins.attrib import attr

from nose.tools import assert_equals, assert_true, assert_is_not_none, assert_almost_equal,\
    assert_is_instance, assert_not_is_instance
from numpy.testing import assert_array_equal


def wrapped(assertion):
    def func(*args, **kwargs):
        return assertion(*args, **kwargs)
    return func


class _TestMultigridSolver(object):
    @classmethod
    def setup_class(cls):
        try:
            cls.solver = mg.MultigridSolver(cls.dtype, cls.device, cls.compiled)
        except AttributeError:
            cls.solver = mg.MultigridSolver(cls.dtype, cls.device, compiled = False)
            cls.compiled = False
        cls.solver.initialize_hierarchy(cls.Ncoarsest, cls.numGrids)
        cls.solver.set_vcycle_parameters(cls.presmooths, cls.postsmooths, cls.innersmooths)

    def test_converges_to_known_soln(self):
        self.solver.set_initial_solution(0)
        self.solver.set_rhs(0)

        def bcfunc(x, y):
            g = np.cos(np.pi*y/2)*np.sinh(np.pi)
            return np.where(x == 1, g, 0)

        def exactsolnfunc(x, y):
            u = np.cos(np.pi*y/2)*np.sinh(0.5*np.pi*(x+1))
            return u

        self.solver.set_boundary_condition(bcfunc)

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(self.numCycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        level = self.solver.fullHierarchy[-1]
        grid = level.grid
        exactsoln = grid.create_grid_variable(exactsolnfunc)
        numericalsoln = level.u.curr
        err = exactsoln - numericalsoln
        errnorm = grid.compute_grid_norm(err, 2)

        yield wrapped(assert_almost_equal), errnorm, 0, None, None, 0.001

    def test_coarse_grid_initial_guess_starts_as_zero(self):
        self.solver.set_initial_solution(
            lambda x, y: (1-x)*(1+x)*(1-y)*(1+y)
        )
        self.solver.set_rhs(0)
        self.solver.set_boundary_condition(0)
        u0 = self.solver.fullHierarchy[-1].u.curr.copy()

        yield wrapped(assert_true), self.solver.is_ready()

        if self.device:
            self.solver.memcpy_dtoh_all()

        for level in self.solver.fullHierarchy[:-1]:
            yield assert_array_equal, level.u.curr, 0

    def test_coarse_grid_initial_guess_resets_to_zero(self):
        self.solver.set_initial_solution(
            lambda x, y: (1-x)*(1+x)*(1-y)*(1+y)
        )
        self.solver.set_rhs(0)
        self.solver.set_boundary_condition(0)
        u0 = self.solver.fullHierarchy[-1].u.curr.copy()

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(1)

        if self.device:
            self.solver.memcpy_dtoh_all()

        for level in self.solver.fullHierarchy[:-1]:
            yield assert_array_equal, level.u.curr, 0

    def test_converges_to_zero_soln(self):
        self.solver.set_initial_solution(
            lambda x, y: (1-x)*(1+x)*(1-y)*(1+y)
        )
        self.solver.set_rhs(0)
        self.solver.set_boundary_condition(0)
        u0 = self.solver.fullHierarchy[-1].u.curr.copy()

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(self.numCycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        soln = self.solver.fullHierarchy[-1].u.curr
        solnnorm = self.solver.fullHierarchy[-1].grid.compute_grid_norm(soln, np.Inf)

        yield wrapped(assert_almost_equal), solnnorm, 0, None, None, 5e-5

    def test_zero_residual_reverts_to_smoothing(self):
        self.solver._debug_zero_residual = True
        self.solver.set_initial_solution(lambda x, y: np.random.rand(x.shape[0], x.shape[1]))
        self.solver.set_rhs(0)
        self.solver.set_boundary_condition(0)

        yield wrapped(assert_true), self.solver.is_ready()

        level = self.solver.fullHierarchy[-1]
        ucpy = mg.ArrayPair(level.u.curr.copy(), level.u.tmp.copy())

        if not self.device:
            self.solver.smooth(ucpy, level.f, level.grid, self.numCycles*(self.presmooths + self.postsmooths))
        else:
            sm.smooth_weighted_jacobi_device(ucpy, level.f, level.grid,
                                             self.numCycles*(self.presmooths + self.postsmooths))
        self.solver.run_vcycle(self.numCycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        yield assert_array_equal, ucpy.curr, level.u.curr

    def test_converges_to_linear_soln(self):
        exactsolnfunc = lambda x, y: 2.1*x - 3.0*y + 1.4

        self.solver.set_initial_solution(0)
        self.solver.set_rhs(0)
        self.solver.set_boundary_condition(exactsolnfunc)

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(self.numCycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        level = self.solver.fullHierarchy[-1]
        grid = level.grid
        exactsoln = grid.create_grid_variable(exactsolnfunc)
        soln = level.u.curr
        err = soln - exactsoln
        errnorm = self.solver.fullHierarchy[-1].grid.compute_grid_norm(err, np.Inf)

        yield wrapped(assert_almost_equal), errnorm, 0, None, None, 5e-5

    def test_fixed_point(self):
        numvcycles = 1
        exactsolnfunc = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)
        level = self.solver.fullHierarchy[-1]
        grid = level.grid
        exactsoln = grid.create_grid_variable(exactsolnfunc)
        self.solver.set_initial_solution(exactsoln)
        self.solver.set_boundary_condition(exactsolnfunc)
        f = grid.create_grid_variable(0)
        op.apply_minus_laplacian(f, exactsoln, grid)
        self.solver.set_rhs(f)

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(numvcycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        soln = level.u.curr
        err = soln - exactsoln
        errnorm = grid.compute_grid_norm(err, np.Inf)

        yield wrapped(assert_almost_equal), errnorm, 0, None, None, (1e-4 if self.dtype == np.float32 else 1e-13)

    def test_converges_to_bump_soln(self):
        def exactsolnfunc(x, y):
            r = np.sqrt(x**2 + y**2)

            exponent = np.zeros_like(r)
            interior = np.abs(2*r) < 1
            exponent[interior] = -1.0/(1.0 - (2*r[interior])**2)
            values = np.exp(exponent)

            return np.where(interior, values, 0)

        level = self.solver.fullHierarchy[-1]
        grid = level.grid

        exactsoln = grid.create_grid_variable(exactsolnfunc)

        Luexact = grid.create_grid_variable(0)
        op.apply_minus_laplacian(Luexact, exactsoln, grid)

        self.solver.set_initial_solution(0)
        self.solver.set_rhs(Luexact)
        self.solver.set_boundary_condition(0)

        yield wrapped(assert_true), self.solver.is_ready()

        self.solver.run_vcycle(self.numCycles)

        if self.device:
            self.solver.memcpy_dtoh_all()

        soln = level.u.curr
        err = soln - exactsoln
        errnorm = self.solver.fullHierarchy[-1].grid.compute_grid_norm(err, np.Inf)

        yield wrapped(assert_almost_equal), errnorm, 0, None, None, 5e-5


class _TestTwoCycleSolver(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.numGrids = 2

        super(_TestTwoCycleSolver, cls).setup_class()


class TestTwoCycleSolverHostDouble(_TestTwoCycleSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.device = False
        cls.Ncoarsest = 33
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 500
        cls.numCycles = 10

        super(TestTwoCycleSolverHostDouble, cls).setup_class()


class TestMultigridSolverHostDouble(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.device = False
        cls.Ncoarsest = 17
        cls.numGrids = 6
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 100
        cls.numCycles = 10

        super(TestMultigridSolverHostDouble, cls).setup_class()


class TestMultigridSolverHostDoubleCompiled(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.device = False
        cls.compiled = True
        cls.Ncoarsest = 17
        cls.numGrids = 6
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 100
        cls.numCycles = 10

        super(TestMultigridSolverHostDoubleCompiled, cls).setup_class()


class TestTwoCycleSolverHostSingle(_TestTwoCycleSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.device = False
        cls.Ncoarsest = 33
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 500
        cls.numCycles = 10

        super(TestTwoCycleSolverHostSingle, cls).setup_class()


class TestMultigridSolverHostSingle(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.device = False
        cls.Ncoarsest = 17
        cls.numGrids = 6
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 100
        cls.numCycles = 10

        super(TestMultigridSolverHostSingle, cls).setup_class()


class TestMultigridSolverHostSingleCompiled(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64
        cls.device = False
        cls.compiled = True
        cls.Ncoarsest = 17
        cls.numGrids = 6
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 100
        cls.numCycles = 10
        cls.compiled = True

        super(TestMultigridSolverHostSingleCompiled, cls).setup_class()

class TestMultigridSolverDevice(_TestMultigridSolver):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32
        cls.device = True
        cls.Ncoarsest = 5
        cls.numGrids = 9
        cls.presmooths = 3
        cls.postsmooths = 3
        cls.innersmooths = 10
        cls.numCycles = 30

        super(TestMultigridSolverDevice, cls).setup_class()


def test_initialization_host_double():
    dtype = np.float64
    device = False
    Ncoarsest = 5
    numGrids = 5
    u0 = 1
    rhs = lambda x, y: np.power(np.pi,2)*(3*np.cos(np.pi*x) + 16*np.sin(4*np.pi*y))
    uexact = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)
    presmooths = 2
    postsmooths = 3
    innersmooths = 100

    tests = _test_initialization(dtype,
                                 device,
                                 Ncoarsest,
                                 numGrids,
                                 u0,
                                 rhs,
                                 uexact,
                                 presmooths,
                                 postsmooths,
                                 innersmooths)

    for test in tests:
        yield test


def test_initialization_host_single():
    dtype = np.float32
    device = False
    Ncoarsest = 5
    numGrids = 5
    u0 = 1
    rhs = lambda x, y: np.power(np.pi,2)*(3*np.cos(np.pi*x) + 16*np.sin(4*np.pi*y))
    uexact = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)
    presmooths = 2
    postsmooths = 3
    innersmooths = 100

    tests = _test_initialization(dtype,
                                 device,
                                 Ncoarsest,
                                 numGrids,
                                 u0,
                                 rhs,
                                 uexact,
                                 presmooths,
                                 postsmooths,
                                 innersmooths)

    for test in tests:
        yield test


def test_initialization_device():
    dtype = np.float32
    device = True
    Ncoarsest = 5
    numGrids = 5
    u0 = 1
    rhs = lambda x, y: np.power(np.pi,2)*(3*np.cos(np.pi*x) + 16*np.sin(4*np.pi*y))
    uexact = lambda x, y: 3*np.cos(np.pi*x) + np.sin(4*np.pi*y)
    presmooths = 2
    postsmooths = 3
    innersmooths = 100

    tests = _test_initialization(dtype,
                                 device,
                                 Ncoarsest,
                                 numGrids,
                                 u0,
                                 rhs,
                                 uexact,
                                 presmooths,
                                 postsmooths,
                                 innersmooths)

    for test in tests:
        yield test


def _test_initialization(dtype,
                         device,
                         Ncoarsest,
                         numGrids,
                         u0,
                         rhs,
                         uexact,
                         presmooths,
                         postsmooths,
                         innersmooths):

    solver = mg.MultigridSolver(dtype, device)

    yield assert_is_not_none, solver, 'solver initialization did not happen'
    yield assert_equals, solver.dtype, dtype, 'solver.dtype is incorrect'
    yield assert_equals, solver.device, device, 'solver.device is incorrect'

    solver.initialize_hierarchy(Ncoarsest, numGrids)

    yield assert_equals, len(solver.fullHierarchy), numGrids, 'solver.hierarchy length is incorrect'

    for level in solver.fullHierarchy:
        yield assert_equals, level.grid.dtype, dtype
        yield assert_equals, level.u.curr.dtype, dtype

    if device:
        for level in solver.fullHierarchy:
            yield assert_is_instance, level, mg.GridLevelDataDevice, 'solver hierarchy level is not an instance of GridLevelDataDevice'
    else:
        for level in solver.fullHierarchy:
            yield assert_is_instance, level, mg.GridLevelData, 'solver hierarchy level is not an instance of GridLevelData'
            yield assert_not_is_instance, level, mg.GridLevelDataDevice, 'solver hierarchy level is an instance of GridLevelDataDevice'

    solver.set_initial_solution(u0)

    level = solver.fullHierarchy[-1]

    yield assert_array_equal, level.u.curr, u0, 'outer level u.curr was not set to u0'
    yield assert_array_equal, level.u.tmp, u0, 'outer level u.tmp was not set to u0'

    for level in solver.fullHierarchy[:-1]:
        yield assert_array_equal, level.u.curr, 0, 'inner level u.curr was not set to 0'
        yield assert_array_equal, level.u.tmp, 0, 'inner level u.tmp was not set to 0'

    solver.set_rhs(rhs)

    level = solver.fullHierarchy[-1]
    grid = level.grid
    f = grid.create_grid_variable(rhs)

    yield assert_array_equal, level.f, f, 'outer level f is incorrect'

    for level in solver.fullHierarchy[:-1]:
        yield assert_array_equal, level.f, 0, 'inner level f is incorrect'


    solver.set_boundary_condition(uexact)

    level = solver.fullHierarchy[-1]
    grid = level.grid
    uexactarray = grid.create_grid_variable(uexact)

    for u in (level.u.curr, level.u.tmp):
        for bvfunc in (Grid.get_north_boundary_value,
                       Grid.get_east_boundary_value,
                       Grid.get_south_boundary_value,
                       Grid.get_west_boundary_value):
            yield assert_array_equal, bvfunc(u), bvfunc(uexactarray), 'outer boundary is incorrect'

    for level in solver.fullHierarchy[:-1]:
        for u in (level.u.curr, level.u.tmp):
            for bvfunc in (Grid.get_north_boundary_value,
                           Grid.get_east_boundary_value,
                           Grid.get_south_boundary_value,
                           Grid.get_west_boundary_value):
                yield assert_array_equal, bvfunc(u), 0, 'inner boundary is incorrect'

    solver.set_vcycle_parameters(presmooths, postsmooths, innersmooths)

    yield assert_equals, presmooths, solver.presmooths, 'presmooths not set correctly'
    yield assert_equals, postsmooths, solver.postsmooths, 'postsmooths not set correctly'
    yield assert_equals, innersmooths, solver.coarsesmooths, 'coarsesmooths not set correctly'

    yield assert_true, solver.is_ready(), 'solver is not ready'

from types import *
from numpy.testing import assert_array_equal

import numpy as np
from . import cudautil as cu
from .grid import Grid
from . import boundaryconditions as bc

from . import operator as op

import scipy.linalg as la
import scipy.sparse as sparse
import scipy.sparse.linalg as spla

mod = cu.SourceModule("""
    __global__ void smooth_weighted_jacobi(float * uA, float * uB, float * f, float * h_ptr, int * N_ptr){
        int i = blockDim.y*blockIdx.y + threadIdx.y;
        int j = blockDim.x*blockIdx.x + threadIdx.x;

        const float w = 4.0/5.0;

        const float & h = h_ptr[0];
        const int & N = N_ptr[0];

        if(1 <= i && i < N - 1 && 1 <= j && j < N - 1){
            uB[i * N + j] = w*0.25*(h*h*f[i * N + j]
                                  + uA[(i + 1) * N + j]
                                  + uA[(i - 1) * N + j]
                                  + uA[i * N + (j + 1)]
                                  + uA[i * N + (j - 1)])
                                  + (1.0 - w)*uA[i * N + j];
        }
    }
    """)
_smooth_weighted_jacobi_func = mod.get_function('smooth_weighted_jacobi')

def _smooth_jacobi_asserts(u, f, g, its):
    assert isinstance(its, IntType)
    assert its >= 0

    assert isinstance(u, ArrayPair)
    assert u.curr.dtype == f.dtype
    assert f.dtype == g.dtype

    assert u.curr.shape == f.shape
    assert f.shape == (g.N, g.N)


def smooth_weighted_jacobi_device(u, f, g, its = 1):
    _smooth_jacobi_asserts(u, f, g, its)

    assert u.curr.dtype == np.float32

    h = g.h
    N = g.N
    h2 = np.power(h, 2)

    ucurr_d = cu.cuda.mem_alloc(u.curr.nbytes)
    utmp_d = cu.cuda.mem_alloc(u.curr.nbytes)
    f_d = cu.cuda.mem_alloc(f.nbytes)

    u_d = ArrayPair(ucurr_d, utmp_d)

    cu.cuda.memcpy_htod(u_d.curr, u.curr)
    cu.cuda.memcpy_htod(u_d.tmp, u.tmp)
    cu.cuda.memcpy_htod(f_d, f)

    h_d = cu.float_to_device(h)
    N_d = cu.int_to_device(N)

    for clock in xrange(its):
        _smooth_weighted_jacobi_func(u_d.curr, u_d.tmp, f_d, h_d, N_d, block=cu.BLOCK_SIZE, grid=cu.compute_grid(u.curr))
        u_d.swap()
        u.swap()

    cu.cuda.memcpy_dtoh(u.curr, u_d.curr)
    cu.cuda.memcpy_dtoh(u.tmp, u_d.tmp)


# def form_matrix_premultiplied(u, f, g):
#     assert isinstance(u, ArrayPair)
#     assert u.curr.dtype == f.dtype
#     assert f.dtype == g.dtype
#
#     assert u.curr.shape == f.shape
#     assert f.shape == (g.N, g.N)
#
#     L = sparse.lil_matrix((g.N**2, g.N**2), dtype = g.dtype)
#     for i in xrange(1,g.N-1):
#         for j in xrange(1,g.N-1):
#             L[i * g.N + j, i * g.N + j] = 4.0
#             L[i * g.N + j, (i + 1) * g.N + j] = -1.0
#             L[i * g.N + j, (i - 1) * g.N + j] = -1.0
#             L[i * g.N + j, i * g.N + j - 1] = -1.0
#             L[i * g.N + j, i * g.N + j + 1] = -1.0
#
#     for i in xrange(g.N**2):
#         if L[i, i] == 0:
#             L[i, i] = 1
#
#     return L.tocsr()
#
#
# def solve_directly(u, f, g, L):
#     uvec = u.curr.reshape(-1)
#     fvec = f.reshape(-1)
#
#     uvec[:] = spla.spsolve(L, fvec)


def smooth_weighted_jacobi(u, f, g, its = 1):
    _smooth_jacobi_asserts(u, f, g, its)

    w = 4.0/5.0

    h = g.h
    N = g.N
    h2 = h**2

    for clock in xrange(its):
        for i in xrange(1, N-1):
            for j in xrange(1, N-1):
                u.tmp[i, j] = w * 0.25*(h2*f[i, j] + u.curr[i+1, j] + u.curr[i-1, j] +
                                        u.curr[i, j+1] + u.curr[i, j-1])\
                              + (1 - w)*u.curr[i, j]
        u.swap()


def smooth_gslex(u, f, g, its = 1):
    _smooth_jacobi_asserts(u, f, g, its)

    h = g.h
    N = g.N
    h2 = h**2

    for clock in xrange(its):
        for i in xrange(1, N-1):
            for j in xrange(1, N-1):
                u.curr[i, j] = 0.25*(h2*f[i, j] + u.curr[i+1, j] + u.curr[i-1, j] + u.curr[i, j+1] + u.curr[i, j-1])

def smooth_gsrb(u, f, g, its = 1):
    _smooth_jacobi_asserts(u, f, g, its)

    h = g.h
    N = g.N
    h2 = h**2.0

    for clock in xrange(its):
        for k in [0, 1]:
            for i in xrange(1, N-1):
                for j in xrange(1, N-1):
                    if(((i + j) % 2) == k):
                        u.curr[i, j] = 0.25*(h2*f[i, j] + u.curr[i+1, j] + u.curr[i-1, j] +
                                                u.curr[i, j+1] + u.curr[i, j-1])


class ArrayPair(object):
    def __init__(self, curr, tmp):
        assert type(curr) == type(tmp)
        if isinstance(curr, np.ndarray):
            assert curr.shape == tmp.shape
            assert curr.dtype == tmp.dtype
        self.curr = curr
        self.tmp = tmp
    def swap(self):
        self.curr, self.tmp = self.tmp, self.curr
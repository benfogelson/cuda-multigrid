from nose.tools import assert_equals, assert_almost_equals, assert_not_equals

from nose import with_setup

import numpy as np

from numpy.testing import assert_array_equal
from numpy.testing.utils import assert_array_compare

from ..grid import Grid

class _TestGrid(object):
    @classmethod
    def setup_class(cls):
        N = 128
        cls.g = Grid(N, dtype=cls.dtype)

        cls.N = cls.g.int(128)

        cls.NhighRes = cls.g.int(2048)
        cls.gHighRes = Grid(cls.NhighRes, dtype=cls.dtype)

    @classmethod
    def teardown_class(cls):
        pass

    def test_compute_h(self):
        assert_equals(self.g.h, self.g.float(self.g.L / (self.N - 1)))

    def test_grid_norm_of_zero(self):
        v = np.zeros((self.N, self.N))
        n = self.g.compute_grid_norm(v, 2)
        assert_equals(n, 0)

    def test_constant_grid_norm(self):
        c = 1.0
        v = c*np.ones((self.N, self.N), dtype=self.dtype)
        p = 5
        n = self.g.compute_grid_norm(v, p)
        assert_almost_equals(c * (4.0 ** (1.0/p)), n)

    def test_max_norm(self):
        v = self.g.float(np.random.rand(self.N, self.N))
        p = np.Inf

        n = self.g.compute_grid_norm(v, p)

        assert_equals(n, np.abs(v).max())

    def test_meshgrid_matches_vectors(self):
        assert np.all(self.g.x == self.g.X[:,0])
        assert np.all(self.g.y == self.g.Y[0,:])

    def test_grid_points_in_range(self):
        assert_equals(self.g.x.min(), self.g.x0)
        assert_equals(self.g.x.max(), self.g.x1)
        assert_equals(self.g.y.min(), self.g.y0)
        assert_equals(self.g.y.max(), self.g.y1)

    def test_sin_cos_norm(self):
        #Test against Mathematica's computation of the two-norm of sin(3x)*cos(5y)
        #NumberForm[N[Integrate[(Sin[3x]*Cos[5y])^2,{x,-1,1},{y,-1,1}]^(1/2)],16]

        exactN = 0.994803333888355

        v = np.sin(3*self.gHighRes.X)*np.cos(5*self.gHighRes.Y)

        n = self.gHighRes.compute_grid_norm(v, 2)

        assert_almost_equals(exactN, n, delta=0.0001)

    def test_get_grid_variable_nbytes(self):
        v = self.g.create_grid_variable()
        nbytes = self.g.get_grid_variable_nbytes()

        assert_equals(v.nbytes, nbytes)

    def test_create_grid_variable_size_correct(self):
        v = self.g.create_grid_variable()

        assert_equals(v.shape, (self.g.N, self.g.N))

    def test_create_grid_variable_dtype_correct(self):
        v = self.g.create_grid_variable()

        assert_equals(v.dtype, self.g.dtype)

    def test_grid_variable_from_const(self):
        c1 = self.g.dtype(np.random.rand())

        v = self.g.create_grid_variable(c1)
        yield assert_array_equal, v, c1

        c2 = self.g.dtype(np.random.rand())
        v = self.g.create_grid_variable(0)
        self.g.set_grid_variable(v, c2)
        yield assert_array_equal, v, c2

    def test_grid_variable_from_array(self):
        check = lambda x, y, z: assert_array_compare(lambda a, b: a != b, x, y, z)

        vExact = self.g.dtype(np.random.rand(self.g.N, self.g.N))
        v = self.g.create_grid_variable(vExact)

        yield assert_array_equal, v, vExact

        vExact[:,:] = 0

        yield check, v, vExact, 'Creating grid variable from array did not copy data.'

        vExact = self.g.dtype(np.random.rand(self.g.N, self.g.N))
        v = self.g.create_grid_variable(vExact)
        self.g.set_grid_variable(v, vExact)

        yield assert_array_equal, v, vExact

        vExact[:,:] = 0

        yield check, v, vExact, 'Setting grid variable from array did not copy data.'

    def test_grid_variable_from_func(self):
        func = lambda x, y: np.cos(x*y)
        vExact = func(self.g.X, self.g.Y)
        v = self.g.create_grid_variable(func)

        yield assert_array_equal, v, vExact

        func = lambda x, y: np.sin(3*x*y)
        vExact = func(self.g.X, self.g.Y)
        v = self.g.create_grid_variable()
        self.g.set_grid_variable(v, func)

        yield assert_array_equal, v, vExact

    def test_compare_neighbor_grids(self):
        gC = Grid(129)
        gF = Grid(257)

        assert np.allclose(gC.x, gF.x[::2])
        assert np.allclose(gC.y, gF.y[::2])

    def test_grid_variable_dtype(self):
        v = self.g.create_grid_variable(1.2)

        assert v.dtype == self.g.dtype

    def test_grid_norm_dtype(self):
        v = self.g.create_grid_variable()
        v[:,:] = np.random.rand(self.N, self.N)

        n = self.g.compute_grid_norm(v, 2)

        assert n.dtype == self.g.dtype

    def test_get_north_x(self):
        x = Grid.get_north_boundary_value(self.g.X)

        assert_array_equal(x, self.g.x)

    def test_get_east_x(self):
        x = Grid.get_east_boundary_value(self.g.X)

        assert_array_equal(x, self.g.x[-1]*np.ones((self.g.N)))

    def test_get_south_x(self):
        x = Grid.get_south_boundary_value(self.g.X)

        assert_array_equal(x, self.g.x)

    def test_get_west_x(self):
        x = Grid.get_west_boundary_value(self.g.X)

        assert_array_equal(x, self.g.x[0]*np.ones((self.g.N)))

    def test_set_north_boundary_value(self):
        u = self.g.create_grid_variable()
        u[:,:] = np.random.rand(self.g.N, self.g.N)
        uExact = np.arange(self.g.N)

        Grid.set_north_boundary_value(u, uExact)

        assert_array_equal(Grid.get_north_boundary_value(u), uExact)

    def test_set_east_boundary_value(self):
        u = self.g.create_grid_variable()
        u[:,:] = np.random.rand(self.g.N, self.g.N)
        uExact = np.arange(self.g.N)

        Grid.set_east_boundary_value(u, uExact)

        assert_array_equal(Grid.get_east_boundary_value(u), uExact)

    def test_set_south_boundary_value(self):
        u = self.g.create_grid_variable()
        u[:,:] = np.random.rand(self.g.N, self.g.N)
        uExact = np.arange(self.g.N)

        Grid.set_south_boundary_value(u, uExact)

        assert_array_equal(Grid.get_south_boundary_value(u), uExact)

    def test_set_west_boundary_value(self):
        u = self.g.create_grid_variable()
        u[:,:] = np.random.rand(self.g.N, self.g.N)
        uExact = np.arange(self.g.N)

        Grid.set_west_boundary_value(u, uExact)

        assert_array_equal(Grid.get_west_boundary_value(u), uExact)

class TestGridDouble(_TestGrid):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float64

        super(TestGridDouble, cls).setup_class()

class TestGridSingle(_TestGrid):
    @classmethod
    def setup_class(cls):
        cls.dtype = np.float32

        super(TestGridSingle, cls).setup_class()

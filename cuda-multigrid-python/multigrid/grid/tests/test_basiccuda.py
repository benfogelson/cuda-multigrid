from nose.tools import assert_equals, assert_almost_equals, with_setup

import numpy as np

from .. import basiccuda as bc

class TestDoubleMatrix(object):
    def test_small_square(self):
        N = 32
        Ain = np.ones((N, N))
        Ain = Ain.astype(np.float32)
        Aout = np.empty_like(Ain)

        bc.double_matrix(Ain, Aout)

        assert np.allclose(2*Ain, Aout)

    def test_small_nonsquare(self):
        rows = 32
        cols = 16

        Ain = np.random.rand(rows, cols)
        Ain = Ain.astype(np.float32)
        Aout = np.empty_like(Ain)

        bc.double_matrix(Ain, Aout)

        assert np.allclose(2*Ain, Aout)

    def test_large_square(self):
        N = 512
        Ain = np.random.rand(N, N)
        Ain = Ain.astype(np.float32)
        Aout = np.empty_like(Ain)

        bc.double_matrix(Ain, Aout)

        assert np.allclose(2*Ain, Aout)

    def test_large_nonsquare(self):
        rows = 571
        cols = 913

        Ain = np.random.rand(rows, cols)
        Ain = Ain.astype(np.float32)
        Aout = np.empty_like(Ain)

        bc.double_matrix(Ain, Aout)

        assert np.allclose(2*Ain, Aout)

class _TestDoubleVector(object):
    def test_small(self):
        N = 128
        vIn = np.random.rand(N)
        vIn = vIn.astype(np.float32)
        vOut = np.empty_like(vIn)

        bc.double_vector(vIn, vOut)

        assert np.allclose(2*vIn, vOut)

    def test_medium(self):
        N = 1024
        vIn = np.random.rand(N)
        vIn = vIn.astype(np.float32)
        vOut = np.empty_like(vIn)

        bc.double_vector(vIn, vOut)

        assert np.allclose(2*vIn, vOut)

    def test_large(self):
        N = 4096
        vIn = np.random.rand(N)
        vIn = vIn.astype(np.float32)
        vOut = np.empty_like(vIn)

        bc.double_vector(vIn, vOut)

        assert np.allclose(2*vIn, vOut)
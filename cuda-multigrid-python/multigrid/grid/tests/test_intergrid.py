from nose.tools import assert_equals, assert_almost_equals, with_setup
from nose.plugins.attrib import attr

import unittest

import numpy as np
from numpy.testing import assert_array_almost_equal

from ..grid import Grid

from .. import intergrid

from multigrid import compiledhost as ch

@attr('slow')
class _TestInterpolate(object):
    @classmethod
    def setup_class(cls):
        nF = 257
        nC = 129

        gF = Grid(nF, cls.dtype)
        gC = Grid(nC, cls.dtype)

        cls.gF = gF
        cls.gC = gC

    @classmethod
    def teardown_class(cls):
        pass

    def test_coarse_var_is_unmodified(self):
        nC = self.gC.N
        vC = self.gC.create_grid_variable()
        vC[:,:] = np.random.rand(nC, nC).astype(self.gC.dtype)
        vCOrig = vC.copy()
        vF = self.gF.create_grid_variable(0)

        self.interpolate(vF, vC)

        assert(np.all(vC == vCOrig))

    def test_constant_interpolation(self):
        c = self.gC.float(5.4321)
        vC = self.gC.create_grid_variable(c)
        vF = self.gF.create_grid_variable(self.gF.float(0))
        nF = self.gF.N

        self.interpolate(vF, vC)

        assert(np.allclose(vF[1:nF-1, 1:nF-1], c))

    def test_linear_function_interpolation(self):
        vF = self.gF.create_grid_variable()
        vC = self.gC.create_grid_variable()
        vFExact = self.gF.create_grid_variable()
        nF = self.gF.N
        XF, YF = self.gF.X, self.gF.Y
        XC, YC = self.gC.X, self.gC.Y
        linear_func = lambda x, y: self.gC.float(7.2*x - 4.3*y + 6.1)
        vC[:,:] = linear_func(XC, YC)
        vFExact[:,:] = linear_func(XF, YF)
        atol = 1.e-8 if self.dtype == np.float64 else 1.e-6

        self.interpolate(vF, vC)

        assert np.allclose(vF[1:nF-1, 1:nF-1], vFExact[1:nF-1, 1:nF-1], atol=atol)


    def test_interpolation_leaves_boundary_alone(self):
        vF = self.gF.create_grid_variable(-1)
        vC = self.gC.create_grid_variable(-1)
        nF = self.gF.N
        XF, YF = self.gF.X, self.gF.Y
        XC, YC = self.gC.X, self.gC.Y
        linear_func = lambda x, y: self.gC.float(7.2*x - 4.3*y + 6.1)
        vC[:,:] = linear_func(XC, YC)
        vF0 = vF.copy()
        atol = 1.e-8 if self.dtype == np.float64 else 1.e-6

        self.interpolate(vF, vC)

        for loc in Grid.ALL_BOUNDARIES:
            yield assert_array_almost_equal, Grid.get_boundary_value(vF, loc), Grid.get_boundary_value(vF0, loc)


    def test_interpolate_dtype(self):
        c = self.gC.float(5.4321)
        vC = self.gC.create_grid_variable(c)
        vF = self.gF.create_grid_variable(self.gF.float(0))
        nF = self.gF.N

        self.interpolate(vF, vC)

        assert vC.dtype == self.gC.dtype and vF.dtype == self.gF.dtype and vC.dtype == self.dtype

@attr('slow')
class _TestRestrict(object):
    @classmethod
    def setup_class(cls):
        nF = 257
        nC = 129

        gF = Grid(nF, cls.dtype)
        gC = Grid(nC, cls.dtype)

        cls.gF = gF
        cls.gC = gC

    @classmethod
    def teardown_class(cls):
        pass

    def test_fine_var_is_unmodified(self):
        nF = self.gF.N
        vF = self.gF.create_grid_variable()
        vF[:,:] = np.random.rand(nF, nF).astype(self.gF.dtype)
        vC = self.gC.create_grid_variable(self.gC.float(0))
        vFOrig = vF.copy()

        self.restrict(vC, vF)

        assert np.all(vF == vFOrig)

    def test_constant_restriction(self):
        c = self.gC.float(1.2345)
        vF = self.gF.create_grid_variable(c)
        vC = self.gC.create_grid_variable(0)
        nC = self.gC.N

        self.restrict(vC, vF)

        assert np.allclose(vC[1:nC-1, 1:nC-1], c)

    def test_linear_function_restriction(self):
        vF = self.gF.create_grid_variable()
        vC = self.gC.create_grid_variable()
        vCExact = self.gC.create_grid_variable()
        nC = self.gC.N
        XF, YF = self.gF.X, self.gF.Y
        XC, YC = self.gC.X, self.gC.Y
        linear_func = lambda x, y: self.gC.float(7.2*x - 4.3*y + 6.1)
        vF[:,:] = linear_func(XF, YF)
        vCExact[:,:] = linear_func(XC, YC)
        atol = 1.e-8 if self.dtype == np.float64 else 1.e-6

        self.restrict(vC, vF)

        assert np.allclose(vC[1:nC-1, 1:nC-1], vCExact[1:nC-1, 1:nC-1], atol=atol)

    def test_restrict_leaves_boundary_alone(self):
        vF = self.gF.create_grid_variable(-1)
        vC = self.gC.create_grid_variable(-1)
        vC0 = vC.copy()
        nC = self.gC.N
        XF, YF = self.gF.X, self.gF.Y
        XC, YC = self.gC.X, self.gC.Y
        linear_func = lambda x, y: self.gC.float(7.2*x - 4.3*y + 6.1)
        vF[:,:] = linear_func(XF, YF)

        self.restrict(vC, vF)

        for loc in Grid.ALL_BOUNDARIES:
            yield assert_array_almost_equal, Grid.get_boundary_value(vC, loc), Grid.get_boundary_value(vC0, loc)

    def test_restrict_dtype(self):
        c = self.gC.float(1.2345)
        vF = self.gF.create_grid_variable(c)
        vC = self.gC.create_grid_variable(0)
        nC = self.gC.N

        self.restrict(vC, vF)

        assert vC.dtype == self.gC.dtype and vF.dtype == self.gF.dtype and vC.dtype == self.dtype

class TestInterpolateHostDouble(_TestInterpolate):
    @classmethod
    def setup_class(cls):
        cls.interpolate = staticmethod(intergrid.interpolate)
        cls.dtype = np.float64

        super(TestInterpolateHostDouble, cls).setup_class()

class TestInterpolateHostDoubleCompiled(_TestInterpolate):
    @classmethod
    def setup_class(cls):
        cls.interpolate = staticmethod(ch.interpolate_double)
        cls.dtype = np.float64

        super(TestInterpolateHostDoubleCompiled, cls).setup_class()

class TestInterpolateHostSingle(_TestInterpolate):
    @classmethod
    def setup_class(cls):
        cls.interpolate = staticmethod(intergrid.interpolate)
        cls.dtype = np.float32

        super(TestInterpolateHostSingle, cls).setup_class()

class TestInterpolateHostSingleCompiled(_TestInterpolate):
    @classmethod
    def setup_class(cls):
        cls.interpolate = staticmethod(ch.interpolate_single)
        cls.dtype = np.float32

        super(TestInterpolateHostSingleCompiled, cls).setup_class()

class TestRestrictHostDouble(_TestRestrict):
    @classmethod
    def setup_class(cls):
        cls.restrict = staticmethod(intergrid.restrict)
        cls.dtype = np.float64

        super(TestRestrictHostDouble, cls).setup_class()

class TestRestrictHostDoubleCompiled(_TestRestrict):
    @classmethod
    def setup_class(cls):
        cls.restrict = staticmethod(ch.restrict_double)
        cls.dtype = np.float64

        super(TestRestrictHostDoubleCompiled, cls).setup_class()

class TestRestrictHostSingle(_TestRestrict):
    @classmethod
    def setup_class(cls):
        cls.restrict = staticmethod(intergrid.restrict)
        cls.dtype = np.float32

        super(TestRestrictHostSingle, cls).setup_class()

class TestRestrictHostSingleCompiled(_TestRestrict):
    @classmethod
    def setup_class(cls):
        cls.restrict = staticmethod(ch.restrict_single)
        cls.dtype = np.float32

        super(TestRestrictHostSingleCompiled, cls).setup_class()

class TestRestrictDevice(_TestRestrict):
    @classmethod
    def setup_class(cls):
        cls.restrict = staticmethod(intergrid.restrict_device)
        cls.dtype = np.float32

        super(TestRestrictDevice, cls).setup_class()

class TestInterpolateDevice(_TestInterpolate):
    @classmethod
    def setup_class(cls):
        cls.interpolate = staticmethod(intergrid.interpolate_device)
        cls.dtype = np.float32

        super(TestInterpolateDevice, cls).setup_class()
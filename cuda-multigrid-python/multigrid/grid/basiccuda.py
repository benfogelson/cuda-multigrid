import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import numpy as np

mod = SourceModule("""

    __global__ void times_two_vector(float * vIn, float * vOut, int N)
    {
        int idx = blockIdx.x*blockDim.x + threadIdx.x;
        if(idx < N){
            vOut[idx] = 2*vIn[idx];
        }
    }

    __global__ void times_two_matrix(float * Ain, float * Aout, int * rows_ptr, int * cols_ptr)
    {
        int rows = rows_ptr[0];
        int cols = cols_ptr[0];

        int i = blockDim.y*blockIdx.y + threadIdx.y; // Row number
        int j = blockDim.x*blockIdx.x + threadIdx.x; // Col number

        int index = cols*i + j; // Flattened index

        if(index < rows*cols){
            Aout[index] = 2*Ain[index];
        }
    }
""")

times_two_vector_func = mod.get_function("times_two_vector")
times_two_matrix_func = mod.get_function("times_two_matrix")

def double_matrix(Ain, Aout):
    assert Ain.shape == Aout.shape
    assert Ain.dtype == np.float32
    assert Aout.dtype == np.float32
    assert Ain.ndim == 2

    rows, cols = Ain.shape

    block = (16, 16, 1)

    blockDimX = ((cols % block[0])*block[0] + cols)/block[0]
    blockDimY = ((rows % block[1])*block[1] + rows)/block[1]

    grid = (blockDimX, blockDimY)

    assert blockDimX*block[0] >= cols
    assert blockDimY*block[1] >= rows

    Ain_d = cuda.mem_alloc(Ain.nbytes)
    Aout_d = cuda.mem_alloc(Aout.nbytes)

    cuda.memcpy_htod(Ain_d, Ain)

    rows32 = np.array(np.int32(rows))
    cols32 = np.array(np.int32(cols))

    rows_d = cuda.mem_alloc(rows32.nbytes)
    cols_d = cuda.mem_alloc(cols32.nbytes)

    cuda.memcpy_htod(rows_d, rows32)
    cuda.memcpy_htod(cols_d, cols32)

    times_two_matrix_func(Ain_d, Aout_d, rows_d, cols_d, block=block, grid=grid)

    cuda.memcpy_dtoh(Aout, Aout_d)

def double_vector(vIn, vOut):
    assert vIn.shape == vOut.shape
    assert vIn.dtype == np.float32
    assert vOut.dtype == np.float32
    assert vIn.ndim == 1

    N = len(vIn)

    MAX_BLOCK_LENGTH = 1024

    numBlocks = int(np.ceil(float(N)/float(MAX_BLOCK_LENGTH)))
    blockLength = MAX_BLOCK_LENGTH if N > MAX_BLOCK_LENGTH else N

    vIn_d = cuda.mem_alloc(vIn.nbytes)
    vOut_d = cuda.mem_alloc(vOut.nbytes)

    cuda.memcpy_htod(vIn_d, vIn)

    times_two_vector_func(vIn_d, vOut_d, cuda.In(np.array(np.int32(N))), block=(blockLength, 1, 1), grid=(numBlocks, 1))

    cuda.memcpy_dtoh(vOut, vOut_d)


#a = numpy.ones((16,16))

#a = a.astype(numpy.float32)

#c = numpy.int32(5)

#a_gpu = cuda.mem_alloc(a.nbytes)

#cuda.memcpy_htod(a_gpu, a)



#func = mod.get_function("doublify")
#func(a_gpu, c, block=(8, 8, 1))

#a_doubled = numpy.empty_like(a)
#cuda.memcpy_dtoh(a_doubled, a_gpu)

#print a_doubled
#print a

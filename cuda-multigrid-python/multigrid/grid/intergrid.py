from types import *

import multigrid.cudautil as cu
import numpy as np

mod = cu.SourceModule("""

    __global__ void do_restrict(float * vC, float * vF, int * nC_ptr, int * nF_ptr){

        int iC = blockDim.y*blockIdx.y + threadIdx.y;
        int jC = blockDim.x*blockIdx.x + threadIdx.x;

        int iF = 2*iC;
        int jF = 2*jC;

        const int & nC = nC_ptr[0];
        const int & nF = nF_ptr[0];

        if(iC > 0 && iC < nC - 1 && jC > 0 && jC < nC - 1){
            vC[nC*iC + jC] = (4.0*vF[nF*iF + jF] +
                              2.0*(vF[nF*iF + jF+1] + vF[nF*iF + jF-1] + vF[nF*(iF+1) + jF] + vF[nF*(iF-1) + jF]) +
                              (vF[nF*(iF+1) + jF+1] + vF[nF*(iF+1) + jF-1] + vF[nF*(iF-1) + jF+1] + vF[nF*(iF-1) + jF-1])
                             )/16.0;
        }
    }

    __global__ void do_interpolate(float * vF, float * vC, int * nF_ptr, int * nC_ptr){
        extern __shared__ float sData[];

        const unsigned int & nC = nC_ptr[0];
        const unsigned int & nF = nF_ptr[0];

        const unsigned int n_iC_loc = blockDim.y;
        const unsigned int n_jC_loc = blockDim.x;
        const unsigned int n_iF_loc = 2*n_iC_loc + 1;
        const unsigned int n_jF_loc = 2*n_jC_loc + 1;

        const unsigned int length_of_vC_loc = n_iC_loc * n_jC_loc;
        const unsigned int length_of_vF_loc = n_iF_loc * n_jF_loc;

        float * vC_loc = &(sData[0]);
        float * vF_loc = &(sData[length_of_vC_loc]);

        const unsigned int OVERLAP = 2;

        int iC_loc = threadIdx.y;
        int jC_loc = threadIdx.x;

        int iF_loc = 2*iC_loc;
        int jF_loc = 2*jC_loc;

        int iC = blockIdx.y * (blockDim.y - OVERLAP) + iC_loc;
        int jC = blockIdx.x * (blockDim.x - OVERLAP) + jC_loc;

        int iF = 2*iC;
        int jF = 2*jC;

        vC_loc[iC_loc * n_jC_loc + jC_loc] = vC[iC * nC + jC];

        const float & vCurr = vC_loc[iC_loc * n_jC_loc + jC_loc];

        for(int di = -1; di <= 1; ++di){
            for(int dj = -1; dj <= 1; ++dj){
                if(    0 <= iF_loc + di && iF_loc + di < n_iF_loc
                    && 0 <= jF_loc + dj && jF_loc + dj < n_jF_loc
                  ){
                    vF_loc[(iF_loc + di) * n_jF_loc + (jF_loc + dj)] = 0;
                }
                __syncthreads();
            }
        }

        for(int di = -1; di <= 1; ++di){
            for(int dj = -1; dj <= 1; ++dj){
                if(    0 <= iF_loc + di && iF_loc + di < n_iF_loc
                    && 0 <= jF_loc + dj && jF_loc + dj < n_jF_loc
                  ){
                    vF_loc[(iF_loc + di) * n_jF_loc + (jF_loc + dj)] +=
                        (di == 0 ? 1.0 : 0.5)*(dj == 0 ? 1.0 : 0.5)*vCurr;
                }
                __syncthreads();
            }
        }

        for(int di = -1; di <= 1; ++di){
            for(int dj = -1; dj <= 1; ++dj){
                if(    0 <= iF_loc + di && iF_loc + di < n_iF_loc && 0 < iF + di && iF + di < nF - 1
                    && 0 <= jF_loc + dj && jF_loc + dj < n_jF_loc && 0 < jF + dj && jF + dj < nF - 1
                  ){
                    if(   di <= 0 && dj <= 0
                       || iF_loc + di == n_iF_loc - 1 && dj <= 0
                       || jF_loc + dj == n_jF_loc - 1 && di <= 0
                       || iF_loc + di == n_iF_loc - 1 && jF_loc + dj == n_jF_loc - 1
                       || iF + di == nF - 1 && dj <= 0
                       || jF + dj == nF - 1 && di <= 0
                       || iF + di == nF - 1 && jF + dj == nF - 1
                    ){
                        vF[(iF + di) * nF + (jF + dj)] = vF_loc[(iF_loc + di) * n_jF_loc + (jF_loc + dj)];
                    }
                }
                __syncthreads();
            }
        }

    }
    """)

_restrict_func = mod.get_function('do_restrict')
_interpolate_func = mod.get_function('do_interpolate')

def _restrict_asserts(vC, vF):
    nF, _ = vF.shape
    nC, _ = vC.shape

    assert vF.dtype == vC.dtype
    assert isinstance(nF, IntType)
    assert isinstance(nC, IntType)
    assert nF == 2*nC - 1, 'Wrong sizes for restriction'


def restrict(vC, vF):
    _restrict_asserts(vC, vF)

    nF, _ = vF.shape
    nC, _ = vC.shape

    for iC in xrange(1, nC-1):
        iF = 2*iC
        for jC in xrange(1, nC-1):
            jF = 2*jC
            vC[iC, jC] = (4.0 * vF[iF, jF]
                          + 2.0 * (vF[iF + 1, jF] + vF[iF - 1, jF] + vF[iF, jF + 1] + vF[iF, jF - 1])
                          + vF[iF + 1, jF + 1] + vF[iF + 1, jF - 1] + vF[iF - 1, jF + 1] + vF[iF - 1, jF - 1])
            vC[iC, jC] = vC[iC, jC]/16.0

def restrict_device(vC, vF):
    _restrict_asserts(vC, vF)

    nF, _ = vF.shape
    nC, _ = vC.shape

    vF_d = cu.cuda.mem_alloc(vF.nbytes)
    vC_d = cu.cuda.mem_alloc(vC.nbytes)

    cu.cuda.memcpy_htod(vF_d, vF)
    cu.cuda.memcpy_htod(vC_d, vC)

    _restrict_func(vC_d, vF_d, cu.int_to_device(nC), cu.int_to_device(nF),
                   block = cu.BLOCK_SIZE, grid = cu.compute_grid(vC))

    cu.cuda.memcpy_dtoh(vC, vC_d)


def interpolate_device(vF, vC):
    _interpolate_asserts(vF, vC)

    nF, _ = vF.shape
    nC, _ = vC.shape

    vF_d = cu.cuda.mem_alloc(vF.nbytes)
    vC_d = cu.cuda.mem_alloc(vC.nbytes)

    cu.cuda.memcpy_htod(vC_d, vC)
    cu.cuda.memcpy_htod(vF_d, vF)

    _interpolate_func(vF_d, vC_d, cu.int_to_device(nF), cu.int_to_device(nC),
                   block = cu.BLOCK_SIZE, grid = cu.compute_overlapped_grid(vC),
                      shared = cu.INTERGRID_SHARED_MEMORY_SIZE)

    cu.cuda.memcpy_dtoh(vF, vF_d)

def _interpolate_asserts(vF, vC):
    nF, _ = vF.shape
    nC, _ = vC.shape

    assert vF.dtype == vC.dtype
    assert isinstance(nF, IntType)
    assert isinstance(nC, IntType)
    assert nF == 2*nC - 1, 'Wrong sizes for interpolation'


def interpolate(vF, vC):
    _interpolate_asserts(vF, vC)

    nF, _ = vF.shape
    nC, _ = vC.shape

    vF[1:-1, 1:-1] = 0

    is_in_interior = lambda ind_i, ind_j: (0 < ind_i < nF - 1 and 0 < ind_j < nF - 1)

    diAll = [-1, 0, 1]
    djAll = [-1, 0, 1]

    nbrStencil = [(di, dj, np.power(0.5, np.abs(di))*np.power(0.5, np.abs(dj))) for di in diAll for dj in djAll]

    for iC in xrange(0, nC):
        iF = 2*iC

        for jC in xrange(0, nC):
            jF = 2*jC

            interiorNbrs = filter(lambda nbr: is_in_interior(iF + nbr[0], jF + nbr[1]), nbrStencil)
            for nbr in interiorNbrs:
                vF[iF + nbr[0], jF + nbr[1]] += nbr[2] * vC[iC, jC]

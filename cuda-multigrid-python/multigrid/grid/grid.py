from types import *

import numpy as np

class Grid(object):
    def __init__(self, N, dtype=np.float64):
        """
        Creates a two-dimensional grid spanning -1 to 1 in the x and y directions

        :param N: Number of grid nodes in each of the x and y directions including boundaries

        :type N: int
        """
        assert isinstance(N, IntType) or isinstance(N, np.int32) or isinstance(N, np.int64)
        assert N > 3

        assert (dtype == np.float32 or dtype == np.float64)

        self.dtype = dtype
        self.float = dtype
        self.int = np.int32 if dtype == np.float32 else np.int64

        self.N = self.int(N)

        self.L = self.float(2.0)
        self.x0 = self.float(-1.0)
        self.x1 = self.float(1.0)
        self.y0 = self.float(-1.0)
        self.y1 = self.float(1.0)

        assert self.x1 - self.x0 == self.L
        assert self.y1 - self.y0 == self.L

        self._compute_h()

        self._compute_grid_points()

        assert self.h.dtype == self.dtype
        assert self.N.dtype == self.int

        assert self.x.dtype == dtype
        assert self.y.dtype == dtype
        assert self.X.dtype == dtype
        assert self.Y.dtype == dtype

        self._N64 = int(self.N)
        self._h64 = float(self.h)

    def get_grid_variable_nbytes(self):
        return self.X.nbytes

    def set_grid_variable(self, v, vVal):
        if hasattr(vVal, '__call__'):
            X = self.X
            Y = self.Y
            v[:,:] = vVal(X, Y)
        elif v is not None:
            v[:,:] = vVal

        assert np.shape(v) == (self.N, self.N)
        assert v.dtype == self.dtype


    def create_grid_variable(self, vVal = None):
        v = np.empty((self.N, self.N), dtype=self.dtype)

        if vVal is not None:
            self.set_grid_variable(v, vVal)

        assert np.shape(v) == (self.N, self.N)
        assert v.dtype == self.dtype

        return v

    def _compute_h(self):
        h = self.L / (self.N - 1)
        self.h = self.float(h)

    def _compute_grid_points(self):
        x, hx = np.linspace(self.x0, self.x1, self.N, True, True, dtype=self.dtype)
        y, hy = np.linspace(self.y0, self.y1, self.N, True, True, dtype=self.dtype)

        X, Y = np.meshgrid(x, y, indexing='ij')

        if X.dtype != self.dtype or Y.dtype != self.dtype:
            X = X.astype(self.dtype)
            Y = Y.astype(self.dtype)

        assert self.float(hx) == self.h
        assert self.float(hy) == self.h

        assert len(x) == self.N
        assert len(y) == self.N

        assert np.shape(X) == (self.N, self.N)
        assert np.shape(Y) == (self.N, self.N)

        assert np.all(x == X[:,0])
        assert np.all(y == Y[0,:])

        self.x = x
        self.y = y
        self.X = X
        self.Y = Y

    def compute_grid_norm(self, v, p):
        '''

        :param v: Grid function
        :param p: Lp norm
        :return: p-norm of v
        '''

        h = self.h
        N = self.N

        assert h > 0
        assert p >= 1
        assert v.shape == (N, N)

        if p == np.Inf:
            n = np.abs(v).max()
        else:
            integrand = np.abs(v) ** p
            rowSum = h*(np.sum(integrand[1:N-1], 0) + 0.5*(integrand[:,0] + integrand[:,-1]))
            totalSum = h*(np.sum(rowSum[1:N-1]) + 0.5*(rowSum[0] + rowSum[-1]))

            n = (totalSum)**(1.0/p)

        n = self.float(n)

        assert n.dtype == self.dtype
        assert n >= 0, 'Computed norm was ' + str(n)
        assert n > 0 or np.all(v == 0)

        return n

    NORTH = 0
    SOUTH = 1
    EAST  = 2
    WEST  = 3
    _VALID_LOCATIONS = [NORTH, SOUTH, EAST, WEST]
    ALL_BOUNDARIES = _VALID_LOCATIONS

    @staticmethod
    def get_west_boundary_value(v):
        return v[0,:]

    @staticmethod
    def get_east_boundary_value(v):
        return v[-1,:]

    @staticmethod
    def get_north_boundary_value(v):
        return v[:,0]

    @staticmethod
    def get_south_boundary_value(v):
        return v[:,-1]

    @staticmethod
    def set_north_boundary_value(v, v_boundary):
        Grid.get_north_boundary_value(v)[:] = v_boundary

    @staticmethod
    def set_south_boundary_value(v, v_boundary):
        Grid.get_south_boundary_value(v)[:] = v_boundary

    @staticmethod
    def set_east_boundary_value(v, v_boundary):
        Grid.get_east_boundary_value(v)[:] = v_boundary

    @staticmethod
    def set_west_boundary_value(v, v_boundary):
        Grid.get_west_boundary_value(v)[:] = v_boundary

    @staticmethod
    def get_boundary_value(v, loc):
        assert loc in Grid._VALID_LOCATIONS

        if loc == Grid.NORTH:
            return Grid.get_north_boundary_value(v)
        elif loc == Grid.EAST:
            return Grid.get_east_boundary_value(v)
        elif loc == Grid.SOUTH:
            return Grid.get_south_boundary_value(v)
        elif loc == Grid.WEST:
            return Grid.get_west_boundary_value(v)

    @staticmethod
    def set_boundary_value(v, v_boundary, loc):
        assert loc in Grid._VALID_LOCATIONS

        if loc == Grid.NORTH:
            Grid.set_north_boundary_value(v, v_boundary)
        elif loc == Grid.EAST:
            Grid.set_east_boundary_value(v, v_boundary)
        elif loc == Grid.SOUTH:
            Grid.set_south_boundary_value(v, v_boundary)
        elif loc == Grid.WEST:
            Grid.set_west_boundary_value(v, v_boundary)

    @staticmethod
    def get_all_boundary_values(v):
        n = Grid.get_north_boundary_value(v)
        e = Grid.get_east_boundary_value(v)
        s = Grid.get_south_boundary_value(v)
        w = Grid.get_west_boundary_value(v)

        return n, e, s, w

    @staticmethod
    def is_valid_boundary(loc):
        return (loc in Grid._VALID_LOCATIONS)
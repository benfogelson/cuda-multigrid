from types import *

from .grid import Grid

import numpy as np
from nose.tools import assert_in, assert_is_instance, assert_true

def apply_dirichlet_boundary_condition(u, g, bdry, locations = Grid.ALL_BOUNDARIES):
    assert u.dtype == g.dtype

    bcIsFunction = hasattr(bdry, '__call__')

    try:
        iter(locations)
    except TypeError:
        locations = [locations]

    for loc in locations:
        assert_true(Grid.is_valid_boundary(loc))
    numTypes = (IntType, FloatType, np.int32, np.int64, np.float32, np.float64)
    typeChecks = [isinstance(bdry, t) for t in numTypes]
    assert (True in typeChecks) or bcIsFunction

    for loc in locations:
        if bcIsFunction:
            x = Grid.get_boundary_value(g.X, loc)
            y = Grid.get_boundary_value(g.Y, loc)
            Grid.set_boundary_value(u, bdry(x, y), loc)
        else:
            Grid.set_boundary_value(u, bdry, loc)
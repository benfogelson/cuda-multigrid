def interpolate_double(vF, vC): pass
def interpolate_single(vF, vC): pass
def restrict_double(vC, vF): pass
def restrict_single(vC, vF): pass
def smooth_weighted_jacobi_double(u, f, g, its = 1): pass
def smooth_weighted_jacobi_single(u, f, g, its = 1): pass
def apply_minus_laplacian_double(Lu, u, g): pass
def apply_minus_laplacian_single(Lu, u, g): pass
def compute_residual_double(r, u, f, g): pass
def compute_residual_single(r, u, f, g): pass
import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import numpy as np

from types import *

BLOCK_SIZE = (16, 16, 1)

SIZE_OF_FLOAT = np.array(1, dtype=np.float32).nbytes

_vF_loc_size = SIZE_OF_FLOAT * (2*BLOCK_SIZE[0] + 1)*(2*BLOCK_SIZE[1] + 1)
_vC_loc_size = SIZE_OF_FLOAT * (BLOCK_SIZE[0]) * (BLOCK_SIZE[1]);
INTERGRID_SHARED_MEMORY_SIZE = _vC_loc_size + _vF_loc_size

def float_to_device(x):
    assert isinstance(x, FloatType) or isinstance(x, np.float64) or isinstance(x, np.float32)

    x32 = np.array(np.float32(x))

    x_d = cuda.mem_alloc(x32.nbytes)

    cuda.memcpy_htod(x_d, x32)

    return x_d

def int_to_device(N):
    assert isinstance(N, IntType) or isinstance(N, np.int64) or isinstance(N, np.int32)

    N32 = np.array(np.int32(N))

    N_d = cuda.mem_alloc(N32.nbytes)

    cuda.memcpy_htod(N_d, N32)

    return N_d

def compute_overlapped_grid(A, block=BLOCK_SIZE, overlap=2):
    assert 2*overlap < block[0]
    assert 2*overlap < block[1]

    N, M = A.shape

    Nmod = (N - overlap) % (block[0] - overlap)
    blockDimX = (N - overlap - Nmod)/(block[0] - overlap) + (1 if Nmod > 0 else 0)

    Mmod = (M - overlap) % (block[1] - overlap)
    blockDimY = (M - overlap - Mmod)/(block[1] - overlap) + (1 if Mmod > 0 else 0)

    grid = (blockDimX, blockDimY)

    return grid

def compute_grid(A, block=BLOCK_SIZE):
    N, M = A.shape

    Nmod = N % block[0]
    blockDimX = (N - Nmod)/block[0] + (1 if Nmod > 0 else 0)

    Mmod = M % block[1]
    blockDimY = (M - Mmod)/block[1] + (1 if Mmod > 0 else 0)

    grid = (blockDimX, blockDimY)

    return grid
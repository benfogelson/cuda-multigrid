import numpy as np


def gslex(u, f, h):
    N, _ = u.shape

    for i in xrange(1,N-1):
        for j in xrange(1,N-1):
            u[i,j] = ((h**2)*f[i,j] + u[i+1,j] + u[i-1,j] + u[i,j+1] + u[i,j-1])/4.0

    return u


def weighted_jacobi(u, f, h):
    N, _ = u.shape
    unew = u.copy()

    w = 4.0/5.0

    for i in xrange(1,N-1):
        for j in xrange(1,N-1):
            unew[i,j] = (w*((h**2)*f[i,j] + u[i+1,j] + u[i-1,j] + u[i,j+1] + u[i,j-1])/4.0) + (1.0 - w)*u[i,j]

    return unew


smooth = weighted_jacobi


def interpolate(u_c):
    N_c, _ = u_c.shape
    N_f = 2 * N_c - 1

    u_f = np.zeros((N_f, N_f))

    for i_c in xrange(1, N_c - 1):
        i_f = 2 * i_c
        for j_c in xrange(1, N_c - 1):
            j_f = 2 * j_c
            u_f[i_f, j_f] = u_c[i_c, j_c]
            u_f[i_f + 1, j_f] += 0.5 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f] += 0.5 * u_c[i_c, j_c]
            u_f[i_f, j_f + 1] += 0.5 * u_c[i_c, j_c]
            u_f[i_f, j_f - 1] += 0.5 * u_c[i_c, j_c]
            u_f[i_f + 1, j_f + 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f + 1, j_f - 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f + 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f - 1] += 0.25 * u_c[i_c, j_c]

    return u_f


def restrict(u_f):
    N_f, _ = u_f.shape
    N_c = (N_f + 1)/2

    u_c = np.zeros((N_c, N_c))

    for i_c in xrange(1, N_c - 1):
        i_f = 2 * i_c
        for j_c in xrange(1, N_c - 1):
            j_f = 2 * j_c
            u_c[i_c, j_c] = 4.0 * u_f[i_f, j_f] \
                            + 2.0 * (u_f[i_f + 1, j_f] + u_f[i_f - 1, j_f] +
                                     u_f[i_f, j_f + 1] + u_f[i_f, j_f - 1]) \
                            + (u_f[i_f + 1, j_f + 1] + u_f[i_f + 1, j_f - 1] +
                               u_f[i_f - 1, j_f + 1] + u_f[i_f - 1, j_f - 1])
            u_c[i_c, j_c] = u_c[i_c, j_c]/16.0

    return u_c


def residual(u, f, h):
    N, _ = u.shape

    r = np.zeros((N, N))

    for i in xrange(1, N-1):
        for j in xrange(1, N-1):
            r[i, j] = f[i, j] - (1.0/(h*h))*(4*u[i, j] - u[i + 1, j] - u[i - 1, j] - u[i, j + 1] - u[i, j - 1])

    return r


def solve(u0, f, presmooths, postsmooths, coarsesmooths, Ncoarsest):
    u = u0.copy()
    N, _ = u.shape
    h = 2.0/(N - 1)
    if N != Ncoarsest:
        for _ in xrange(presmooths):
            u = smooth(u, f, h)
        r = residual(u, f, h)
        r_c = restrict(r)
        e_c0 = 0*r_c
        e_c = solve(e_c0, r_c, presmooths, postsmooths, coarsesmooths, Ncoarsest)
        e = interpolate(e_c)
        u = u + e
        for _ in xrange(postsmooths):
            u = smooth(u, f, h)
        return u
    else:
        for _ in xrange(coarsesmooths):
            u = smooth(u, f, h)
        return u


def gslex_inplace(u, f, h):
    N, _ = u.shape

    for i in xrange(1,N-1):
        for j in xrange(1,N-1):
            u[i,j] = ((h**2)*f[i,j] + u[i+1,j] + u[i-1,j] + u[i,j+1] + u[i,j-1])/4.0


def weighted_jacobi_inplace(u, f, h, u_temp = None):
    N, _ = u.shape

    if u_temp is None:
        u_temp = u.copy()

    w = 4.0/5.0

    for i in xrange(1,N-1):
        for j in xrange(1,N-1):
            u[i,j] = (w*((h**2)*f[i,j] + u_temp[i+1,j] + u_temp[i-1,j] + u_temp[i,j+1] + u_temp[i,j-1])/4.0) + (1.0 - w)*u_temp[i,j]


def interpolate_inplace(u_f, u_c):
    N_c, _ = u_c.shape
    N_f = 2 * N_c - 1

    u_f[:, :] = 0

    for i_c in xrange(1, N_c - 1):
        i_f = 2 * i_c
        for j_c in xrange(1, N_c - 1):
            j_f = 2 * j_c
            u_f[i_f, j_f] = u_c[i_c, j_c]
            u_f[i_f + 1, j_f] += 0.5 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f] += 0.5 * u_c[i_c, j_c]
            u_f[i_f, j_f + 1] += 0.5 * u_c[i_c, j_c]
            u_f[i_f, j_f - 1] += 0.5 * u_c[i_c, j_c]
            u_f[i_f + 1, j_f + 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f + 1, j_f - 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f + 1] += 0.25 * u_c[i_c, j_c]
            u_f[i_f - 1, j_f - 1] += 0.25 * u_c[i_c, j_c]


def restrict_inplace(u_c, u_f):
    N_f, _ = u_f.shape
    N_c = (N_f + 1)/2

    u_c[:,:] = 0

    for i_c in xrange(1, N_c - 1):
        i_f = 2 * i_c
        for j_c in xrange(1, N_c - 1):
            j_f = 2 * j_c
            u_c[i_c, j_c] = 4.0 * u_f[i_f, j_f] \
                            + 2.0 * (u_f[i_f + 1, j_f] + u_f[i_f - 1, j_f] +
                                     u_f[i_f, j_f + 1] + u_f[i_f, j_f - 1]) \
                            + (u_f[i_f + 1, j_f + 1] + u_f[i_f + 1, j_f - 1] +
                               u_f[i_f - 1, j_f + 1] + u_f[i_f - 1, j_f - 1])
            u_c[i_c, j_c] = u_c[i_c, j_c]/16.0


def residual_inplace(r, u, f, h):
    N, _ = u.shape

    r[:, :] = 0

    for i in xrange(1, N-1):
        for j in xrange(1, N-1):
            r[i, j] = f[i, j] - (1.0/(h*h))*(4*u[i, j] - u[i + 1, j] - u[i - 1, j] - u[i, j + 1] - u[i, j - 1])


def solve_inplace(u, f, presmooths, postsmooths, coarsesmooths, Ncoarsest):
    N, _ = u.shape
    h = 2.0/(N - 1)
    if N != Ncoarsest:
        N_c = (N + 1)/2
        r_c = np.zeros((N_c, N_c))
        e_c = np.zeros_like(r_c)

        r = np.zeros((N, N))
        e = np.zeros_like(r)

        for _ in xrange(presmooths):
            smooth_inplace(u, f, h)
        residual_inplace(r, u, f, h)
        restrict_inplace(r_c, r)
        solve_inplace(e_c, r_c, presmooths, postsmooths, coarsesmooths, Ncoarsest)
        interpolate_inplace(e, e_c)
        u[:,:] = u + e
        for _ in xrange(postsmooths):
            smooth_inplace(u, f, h)

    else:
        for _ in xrange(coarsesmooths):
            smooth_inplace(u, f, h)


smooth_inplace = weighted_jacobi_inplace
This is a final project for the upper level graduate class *Advanced Topics in Numerical Analysis: High Performance Computing*. The code implements a
two-dimensional multigrid solver for Laplace's equation on a rectangular domain using Cuda extensions for computationally extensive portions of the
algorithm. The file "Fogelson Final Project.pdf" is the final report turned in for the course, which includes timing results comparing CPU to GPU solves
in single and double precision.

